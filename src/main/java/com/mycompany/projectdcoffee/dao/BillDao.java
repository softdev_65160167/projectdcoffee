/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Bill;
import com.mycompany.projectdcoffee.model.BillDetail;
import com.mycompany.projectdcoffee.model.BillReport;
import com.mycompany.projectdcoffee.model.Reciept;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class BillDao {
    public Bill get(int id) {
        Bill bill = null;
        String sql = "SELECT * FROM bill WHERE bill_id= ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                bill = Bill.fromRS(rs);
                BillDetailDao rdd = new BillDetailDao();
                ArrayList<BillDetail> billDetails = (ArrayList<BillDetail>)rdd.getAll("bill_id="+bill.getId()," billDetail_id ");
                bill.setBillDetails(billDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return bill;
    }

    public List<Bill> getAll() {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Bill> getAll(String where, String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Bill> getAll(String order) {
        ArrayList<Bill> list = new ArrayList();
        String sql = "SELECT * FROM bill ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Bill bill = Bill.fromRS(rs);
                list.add(bill);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Bill save(Bill obj) {
        String sql = "INSERT INTO bill (total, cash, bill_totalqty, customer_id, emp_id, member_id, b_id, sc_id, pm_id)"
            + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1,obj.getTotal());
            stmt.setFloat(2,obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getCustomerId());
            stmt.setInt(5, obj.getEmpId());
            stmt.setInt(6, obj.getMemberId());
            stmt.setInt(7, obj.getBranchId());
            stmt.setInt(8, obj.getScId());
            stmt.setInt(9, obj.getPmId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Bill update(Bill obj) {
        String sql = "UPDATE bill"
                + " SET total = ?, cash = ?, bill_totalqty = ?, customer_id = ?, emp_id = ?, member_id = ?, b_id = ?, sc_id = ?, pm_id = ?"
                + " WHERE bill_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1,obj.getTotal());
            stmt.setFloat(2,obj.getCash());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getCustomerId());
            stmt.setInt(5, obj.getEmpId());
            stmt.setInt(6, obj.getMemberId());
            stmt.setInt(7, obj.getBranchId());
            stmt.setInt(8, obj.getScId());
            stmt.setInt(9, obj.getPmId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Bill obj) {
        String sql = "DELETE FROM bill WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
    
    public List<BillReport> getArtistByTotalPrice(int limit) {
        ArrayList<BillReport> list = new ArrayList();
        String sql = """
                    SELECT menu_id,menu_name,menu_price,SUM(qty) as total_qty_ordered, SUM(menu_price * qty) as total
                                                     FROM billDetail  INNER JOIN bill  created_date 
                                                     GROUP BY menu_id,billDetail_id
                                                     HAVING SUM(qty) 
                                                     ORDER BY
                                                     total_qty_ordered DESC
                                                     LIMIT ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BillReport obj = BillReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillReport> getArtistByTotalPrice(String begin, String end, int limit) {
        ArrayList<BillReport> list = new ArrayList();
        String sql = """
                    SELECT     menu_id,menu_name,menu_price,SUM(qty) as total_qty_ordered,SUM(total_price) as total
                      FROM billDetail  INNER JOIN bill ON billDetail.bill_id = bill.bill_id 
                      AND bill.created_date BETWEEN ?  AND ?
                      GROUP BY menu_id, menu_name, menu_price
                      ORDER BY total_qty_ordered DESC
                      LIMIT ?;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                BillReport obj = BillReport.fromRS(rs);
                list.add(obj);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
