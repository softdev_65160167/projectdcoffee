/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class RecieptDao {

    public Reciept get(int id) {
        Reciept reciept = null;
        String sql = "SELECT * FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = Reciept.fromRS(rs);
                RecieptDetailDao rdd = new RecieptDetailDao();
                ArrayList<RecieptDetail> recieptDetails = (ArrayList<RecieptDetail>) rdd.getAll("reciept_id=" + reciept.getId(), " recieptdetail_id");
                reciept.setRecieptDetails(recieptDetails);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    public List<Reciept> getAll() {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getAll(String where, String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Reciept> getAll(String order) {
        ArrayList<Reciept> list = new ArrayList();
        String sql = "SELECT * FROM reciept  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Reciept reciept = Reciept.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Reciept save(Reciept obj) {

        String sql = "INSERT INTO reciept (reciept_orderdate, reciept_indate, reciept_paydate, reciept_materialaddmin, reciept_describe, employee_id, branch_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, (Timestamp) obj.getOrderDate());
            stmt.setTimestamp(2, (Timestamp) obj.getInDate());
            stmt.setTimestamp(3, (Timestamp) obj.getPayDate());
            stmt.setInt(4, obj.getMaterialAddMin());
            stmt.setString(5, obj.getDescribe());
            stmt.setInt(6, obj.getEmployeeId());
            stmt.setInt(7, obj.getBranchId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Reciept update(Reciept obj) {
        String sql = "UPDATE reciept"
                + " SET reciept_orderdate =?, reciept_indate=?, reciept_paydate =?, reciept_materialaddmin =?, reciept_describe =?, employee_id=?, branch_id=?"
                + " WHERE reciept_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setTimestamp(1, (Timestamp) obj.getOrderDate());
            stmt.setTimestamp(2, (Timestamp) obj.getInDate());
            stmt.setTimestamp(3, (Timestamp) obj.getPayDate());
            stmt.setInt(4, obj.getMaterialAddMin());
            stmt.setString(5, obj.getDescribe());
            stmt.setInt(6, obj.getEmployeeId());
            stmt.setInt(7, obj.getBranchId());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println("update error");
            return null;
        }
    }

    public int delete(Reciept obj) {
        String sql = "DELETE FROM reciept WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    private List<Reciept> getSaleData(String sql, String begin, String end, int limit) {
        ArrayList<Reciept> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            if (begin != null && end != null) {
                stmt.setString(1, begin);
                stmt.setString(2, end);
            }
            if (limit > 0) {
                stmt.setInt(3, limit);
            }
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Reciept obj = Reciept.fromRS(rs);
                list.add(obj);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }



// ... (เพิ่มเมทอดอื่นๆ ตามต้องการ)
}
