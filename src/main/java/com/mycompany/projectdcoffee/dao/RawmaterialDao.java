/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Rawmaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class RawmaterialDao implements Dao<Rawmaterial>{
    @Override
    public Rawmaterial get(int id) {
        Rawmaterial rawmaterial = null;
        String sql = "SELECT * FROM rawmaterial WHERE rm_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                rawmaterial = Rawmaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rawmaterial;
    }

    public List<Rawmaterial> getAll() {
        ArrayList<Rawmaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Rawmaterial rawmaterial = Rawmaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Rawmaterial> getAll(String where, String order) {
        ArrayList<Rawmaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Rawmaterial rawmaterial = Rawmaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Rawmaterial> getAll(String order) {
        ArrayList<Rawmaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Rawmaterial rawmaterial = Rawmaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Rawmaterial save(Rawmaterial obj) {

        String sql = "INSERT INTO rawmaterial(rm_name,rm_qoh,rm_price)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getRawName());
            stmt.setInt(2, obj.getRawQoh());
            stmt.setFloat(3, obj.getRawPrice());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Rawmaterial update(Rawmaterial obj) {
        String sql = "UPDATE rawmaterial" 
                + " SET  rm_name= ?,  rm_qoh= ?, rm_price= ?"
                + " WHERE rm_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getRawName());
            stmt.setInt(2, obj.getRawQoh());
            stmt.setFloat(3, obj.getRawPrice());
            stmt.setInt(4, obj.getId());
            
            
            int ret = stmt.executeUpdate();
//            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    public Rawmaterial updateQoh(Rawmaterial obj){
        String sql = "UPDATE rawmaterial"
                + " SET rm_qoh =?, rm_price =?"
                + " WHERE rm_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRawQoh());
            stmt.setFloat(2, obj.getRawPrice());
            stmt.setInt(3, obj.getId());
            
            int ret = stmt.executeUpdate();
//            System.out.println(ret);
            return obj;
        }catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Rawmaterial obj) {
        String sql = "DELETE FROM rawmaterial WHERE rm_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
