/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Fee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class FeeDao {
     public Fee get(int id) {
        Fee checkStockList = null;
        String sql = "SELECT * FROM fee WHERE fee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockList = Fee.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockList;
    }
     
    public List<Fee> getAll() {
        ArrayList<Fee> list = new ArrayList();
        String sql = "SELECT * FROM fee";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Fee fee = Fee.fromRS(rs);
                list.add(fee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Fee> getAll(String where, String order) {
        ArrayList<Fee> list = new ArrayList();
        String sql = "SELECT * FROM fee where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Fee checkStockList = Fee.fromRS(rs);
                list.add(checkStockList);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Fee> getAll(String order) {
        ArrayList<Fee> list = new ArrayList();
        String sql = "SELECT * FROM fee  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Fee checkStockList = Fee.fromRS(rs);
                list.add(checkStockList);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Fee save(Fee obj) {
        String sql = "INSERT INTO fee(fee_month, fee_water, fee_electric, fee_location, fee_total, fee_status)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMonth());
            stmt.setFloat(2, obj.getWater());
            stmt.setFloat(3, obj.getElectric());
            stmt.setFloat(4, obj.getLocation());
            stmt.setFloat(5, obj.getTotal());
            stmt.setString(6, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

        public Fee update(Fee obj) {
        String sql = "UPDATE fee" 
                + " SET  fee_month= ?,  fee_water= ?, fee_electric= ?, fee_location= ?, fee_total= ?, fee_status= ?"
                + " WHERE fee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMonth());
            stmt.setFloat(2, obj.getWater());
            stmt.setFloat(3, obj.getElectric());
            stmt.setFloat(4, obj.getLocation());
            stmt.setFloat(5, obj.getTotal());
            stmt.setString(6, obj.getStatus());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Fee obj) {
        String sql = "DELETE FROM fee WHERE fee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
