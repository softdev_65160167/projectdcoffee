/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.BillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class BillDetailDao {
    public BillDetail get(int id) {
        BillDetail billDetail = null;
        String sql = "SELECT * FROM billDetail WHERE billDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billDetail = BillDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billDetail;
    }
    
    public List<BillDetail> getAllByBillId(int id) {
        BillDetail invoiceDetail;
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM billDetail  WHERE bill_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                invoiceDetail = BillDetail.fromRS(rs);
                list.add(invoiceDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<BillDetail> getAll() {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM billDetail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<BillDetail> getAll(String where, String order) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM billDetail where " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<BillDetail> getAll(String order) {
        ArrayList<BillDetail> list = new ArrayList();
        String sql = "SELECT * FROM billDetail ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillDetail billDetail = BillDetail.fromRS(rs);
                list.add(billDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public BillDetail save(BillDetail obj) {
        String sql = "INSERT INTO billDetail (menu_id, menu_name, menu_price, qty, total_price, bill_id)"
            + " VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMenuId());
            stmt.setString(2,obj.getMenuName());
            stmt.setFloat(3,obj.getMenuPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getBillId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public BillDetail update(BillDetail obj) {
        String sql = "UPDATE billDetail"
                + " SET menu_id =?, menu_name =?, menu_price =?, qty =?, total_price =?, bill_id =?"
                + " WHERE billDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMenuId());
            stmt.setString(2,obj.getMenuName());
            stmt.setFloat(3,obj.getMenuPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setFloat(5, obj.getTotalPrice());
            stmt.setInt(6, obj.getBillId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(BillDetail obj) {
        String sql = "DELETE FROM billDetail WHERE billDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
