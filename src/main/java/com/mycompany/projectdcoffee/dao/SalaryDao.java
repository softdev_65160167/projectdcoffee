/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Salary;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class SalaryDao {
    public Salary get(int id) {
        Salary salary = null;
        String sql = "SELECT * FROM salary WHERE salary_slipid=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salary = Salary.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }

    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(String where, String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<Salary> getAll(String order) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM salary  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Salary save(Salary obj) {

        String sql = "INSERT INTO salary ( emp_id, salary_hour, salary_base, salary_amount, salary_date , salary_note)"
                + "VALUES( ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpid());
            stmt.setInt(2, obj.getShour());
            stmt.setInt(3, obj.getSbase());
            stmt.setInt(4, obj.getSamount());
            stmt.setDate(5, obj.getSdate());
            stmt.setString(6, obj.getSnote());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Salary update(Salary obj) {
        String sql = "UPDATE salary"
                + " SET emp_id=?, salary_hour =?, salary_base =?, salary_amount =?, salary_date =?, salary_note =?"
                + " WHERE salary_slipid = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpid());
            stmt.setInt(2, obj.getShour());
            stmt.setInt(3, obj.getSbase());
            stmt.setInt(4, obj.getSamount());
            stmt.setDate(5, obj.getSdate());
            stmt.setString(6, obj.getSnote());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    


    public int delete(Salary obj) {
        String sql = "DELETE FROM salary WHERE salary_slipid=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
