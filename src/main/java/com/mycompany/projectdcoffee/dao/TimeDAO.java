/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

/**
 *
 * @author chano
 */
import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Time;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TimeDAO {
    private Connection conn;

    public TimeDAO(Connection conn) {
        this.conn = conn;
    }

    public void insertCurrentTime() {
        String sql = "INSERT INTO time (real_time) VALUES (datetime('now', 'localtime'))";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }


    public void resetRealTime() {
        String sql = "UPDATE time SET real_time = datetime('now', 'localtime')";
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void updateRealTime() {
        String sql = "UPDATE time SET real_time = datetime('now', 'localtime')";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Time> getAllTime() {
        List<Time> times = new ArrayList<>();
        String sql = "SELECT * FROM time";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Time time = new Time();
                time.setRealTime(rs.getTimestamp("real_time"));
                times.add(time);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return times;
    }
    
}

