/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.WorkInOut;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chano
 */
/**
 *
 * @author ADMIN
 */
public class WorkInOutDAO {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public WorkInOut get(int id) {
        WorkInOut workinout = null;
        String sql = "SELECT * FROM workin_out WHERE wio_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                workinout = WorkInOut.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return workinout;
    }

    public List<WorkInOut> getAll() {
        ArrayList<WorkInOut> list = new ArrayList();
        String sql = "SELECT * FROM workin_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkInOut workinout = WorkInOut.fromRS(rs);
                list.add(workinout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<WorkInOut> getAll(String where, String order) {
        ArrayList<WorkInOut> list = new ArrayList();
        String sql = "SELECT * FROM workin_out where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkInOut workinout = WorkInOut.fromRS(rs);
                list.add(workinout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<WorkInOut> getAll(String order) {
        ArrayList<WorkInOut> list = new ArrayList();
        String sql = "SELECT * FROM workin_out  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                WorkInOut workinout = WorkInOut.fromRS(rs);
                list.add(workinout);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public boolean save(WorkInOut workInOut) {
        String sql = "INSERT INTO workin_out (emp_id, wio_in, wio_out, wio_date) VALUES (?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, workInOut.getEmpId());
            stmt.setString(2, workInOut.getTimeIn().toString()); // แสดงเวลาเป็น HH:mm:ss
            stmt.setString(3, workInOut.getTimeOut() != null ? workInOut.getTimeOut().toString() : null); // แสดงเวลาเป็น HH:mm:ss
            stmt.setString(4, dateFormat.format(workInOut.getDate())); // แสดงวันที่เป็น "yyyy-MM-dd"
            int rowsInserted = stmt.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean update(WorkInOut workInOut) {
        String sql = "UPDATE workin_out SET wio wio_out = ? WHERE wio_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, workInOut.getTimeOut() != null ? workInOut.getTimeOut().toString() : null); // แสดงเวลาเป็น HH:mm:ss
            statement.setInt(2, workInOut.getId());
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public boolean delete(int id) {
        String sql = "DELETE FROM workin_out WHERE wio_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setInt(1, id);
            int rowsDeleted = statement.executeUpdate();
            return rowsDeleted > 0;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    public int getEmployeeIdByEmployeeName(String employeename) {

        String query = "SELECT emp_id FROM employee WHERE emp_name = ?";
        Connection conn = DatabaseHelper.getConnect();
        int employeeId = 0;
        try (PreparedStatement stmt = conn.prepareStatement(query)) {
            stmt.setString(1, employeename);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                employeeId = rs.getInt("emp_id");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return employeeId;
    }

    public boolean updateWorkInOutByEmployeeAndDate(WorkInOut workInOut) {
        String sql = "UPDATE workin_out SET wio_out = ? WHERE emp_id = ? AND wio_date = ?";
        Connection conn = DatabaseHelper.getConnect();
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, workInOut.getTimeOut().toString());
            statement.setInt(2, workInOut.getEmpId());
            statement.setString(3, dateFormat.format(workInOut.getDate()));
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }
    
    public boolean hasClockInToday(int employeeId) {
        boolean hasClockIn = false;
        String sql = "SELECT COUNT(*) FROM work_in_out WHERE emp_id = ? AND wio_date = ?";
        Connection conn = DatabaseHelper.getConnect();
        try (PreparedStatement preparedStatement = conn.prepareStatement(sql)) {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setDate(2, new Date(System.currentTimeMillis()));

            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                hasClockIn = (count > 0);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return hasClockIn;
    }

}
