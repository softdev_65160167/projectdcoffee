/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.Menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class MenuDao {

    public Menu get(int id) {
        Menu menu = null;
        String sql = "SELECT * FROM menu WHERE menu_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                menu = Menu.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return menu;
    }
    
    public ArrayList<Menu> getAllCategory(int category) {
        ArrayList<Menu> list = new ArrayList<>();
        String sql = "SELECT * FROM menu WHERE category_id = " + category;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Menu pd = Menu.fromRS(rs);
                list.add(pd);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Menu> getAll() {
        ArrayList<Menu> list = new ArrayList();
        String sql = "SELECT * FROM menu";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Menu menu = Menu.fromRS(rs);
                list.add(menu);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Menu> getAll(String where, String order) {
        ArrayList<Menu> list = new ArrayList<>();
        String sql = "SELECT * FROM menu WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Menu menu = Menu.fromRS(rs);
                list.add(menu);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Menu> getAll(String order) {
        ArrayList<Menu> list = new ArrayList();
        String sql = "SELECT * FROM menu  ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Menu menu = Menu.fromRS(rs);
                list.add(menu);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Menu save(Menu obj) {
        String sql = "INSERT INTO menu (menu_name, menu_price, menu_size, menu_sweet_level, menu_type, category_id)"
            + " VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Menu update(Menu obj) {
        String sql = "UPDATE menu"
                + " SET menu_name =?, menu_price=?, menu_size =?, menu_sweet_level =?, menu_type =?, category_id =?"
                + " WHERE menu_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setFloat(2, obj.getPrice());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Menu obj) {
        String sql = "DELETE FROM menu WHERE menu_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
