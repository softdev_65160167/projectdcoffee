/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.PromotionDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class PromotionDetailDao {
    public PromotionDetail get(int id) {
        PromotionDetail promotionDetail = null;
        String sql = "SELECT * FROM promotionDetail WHERE promotionDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotionDetail = PromotionDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotionDetail;
    }

    public List<PromotionDetail> getAll() {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotionDetail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<PromotionDetail> getAll(String where, String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotionDetail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<PromotionDetail> getAll(String order) {
        ArrayList<PromotionDetail> list = new ArrayList();
        String sql = "SELECT * FROM promotionDetail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                PromotionDetail promotionDetail = PromotionDetail.fromRS(rs);
                list.add(promotionDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public PromotionDetail save(PromotionDetail obj) {

        String sql = "INSERT INTO promotionDetail (promotion_id,promotionDetail_Condition,promotionDetail_discount)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPromotionId());
            stmt.setString(2, obj.getCondition());
            stmt.setInt(3,obj.getDiscount());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public PromotionDetail update(PromotionDetail obj) {
        String sql = "UPDATE promotionDetail"
                + " SET promotion_id =?, promotionDetail_Condition=?, promotionDetail_discount =?"
                + " WHERE promotionDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getPromotionId());
            stmt.setString(2, obj.getCondition());
            stmt.setInt(3,obj.getDiscount());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(PromotionDetail obj) {
        String sql = "DELETE FROM promotionDetail WHERE promotionDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
