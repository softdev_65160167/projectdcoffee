/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.CheckStockList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class CheckStockListDao implements Dao<CheckStockList>{
    public CheckStockList get(int id) {
        CheckStockList checkStockList = null;
        String sql = "SELECT * FROM check_stock_list WHERE csl_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockList = CheckStockList.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockList;
    }

    public List<CheckStockList> getAll() {
        ArrayList<CheckStockList> list = new ArrayList();
        String sql = "SELECT * FROM check_stock_list";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockList checkStockList = CheckStockList.fromRS(rs);
                list.add(checkStockList);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckStockList> getAll(String where, String order) {
        ArrayList<CheckStockList> list = new ArrayList();
        String sql = "SELECT * FROM check_stock_list where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockList checkStockList = CheckStockList.fromRS(rs);
                list.add(checkStockList);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckStockList> getAll(String order) {
        ArrayList<CheckStockList> list = new ArrayList();
        String sql = "SELECT * FROM check_stock_list  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockList checkStockList = CheckStockList.fromRS(rs);
                list.add(checkStockList);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public CheckStockList save(CheckStockList obj) {

        String sql = "INSERT INTO check_stock_list(emp_id, csl_expired_material, csl_lost_material, csl_lost_value, csl_totalmaterial, csl_date_check)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setInt(2, obj.getExpiredMaterial());
            stmt.setInt(3, obj.getLostMaterial());
            stmt.setFloat(4, obj.getLostValue());
            stmt.setInt(5, obj.getTotalmaterial());
            stmt.setTimestamp(6, (Timestamp) obj.getDateCheck());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public CheckStockList update(CheckStockList obj) {
        String sql = "UPDATE check_stock_list" 
                + " SET  emp_id= ?,  csl_expired= ?, csl_lost_material= ?, csl_lost_value= ?, csl_totalmaterial= ?, csl_date_check= ?"
                + " WHERE csl_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmpId());
            stmt.setInt(2, obj.getExpiredMaterial());
            stmt.setInt(3, obj.getLostMaterial());
            stmt.setFloat(4, obj.getLostValue());
            stmt.setInt(5, obj.getTotalmaterial());
            stmt.setTimestamp(6, (Timestamp) obj.getDateCheck());
            stmt.setInt(7, obj.getId());
            
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(CheckStockList obj) {
        String sql = "DELETE FROM check_stock_list WHERE csl_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
