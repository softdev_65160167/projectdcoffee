/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.SalaryReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryReportDao {
   public SalaryReport get(int id) {
        SalaryReport salaryReport = null;
        String sql = "SELECT * FROM salaryReport WHERE salaryReport_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salaryReport = SalaryReport.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salaryReport;
    }

    public List<SalaryReport> getAll() {
        ArrayList<SalaryReport> list = new ArrayList();
        String sql = "SELECT * FROM salaryReport";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryReport salaryReport = SalaryReport.fromRS(rs);
                list.add(salaryReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<SalaryReport> getAll(String where, String order) {
        ArrayList<SalaryReport> list = new ArrayList();
        String sql = "SELECT * FROM salaryReport where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryReport salaryReport = SalaryReport.fromRS(rs);
                list.add(salaryReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<SalaryReport> getAll(String order) {
        ArrayList<SalaryReport> list = new ArrayList();
        String sql = "SELECT * FROM salaryReport  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryReport salaryReport = SalaryReport.fromRS(rs);
                list.add(salaryReport);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public SalaryReport save(SalaryReport obj) {

        String sql = "INSERT INTO salaryReport ( salaryDetail_id, emp_id, salaryReport_date, salaryReport_amount)"
                + "VALUES( ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSrdid());
            stmt.setInt(2, obj.getEmpid());
            stmt.setString(3, obj.getSrdate());
            stmt.setInt(4, obj.getSramount());;
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public SalaryReport update(SalaryReport obj) {
        String sql = "UPDATE salaryReport"
                + " SET salaryDetail_id =?, emp_id=?, salaryReport_date =?, salaryReport_amount =?"
                + " WHERE salaryReport_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSrdid());
            stmt.setInt(2, obj.getEmpid());
            stmt.setString(3, obj.getSrdate());
            stmt.setInt(4, obj.getSramount());
            stmt.setInt(5, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(SalaryReport obj) {
        String sql = "DELETE FROM salaryReport WHERE salaryReport_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}

