/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RecieptDetailDao {
    public RecieptDetail get(int id) {
        RecieptDetail recieptdetail = null;
        String sql = "SELECT * FROM recieptdetail WHERE recieptdetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptdetail = RecieptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptdetail;
    }

    public List<RecieptDetail> getAll() {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM recieptdetail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptdetail = RecieptDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<RecieptDetail> getAll(String where, String orecieptdetailer) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM recieptdetail WHERE " + where + " ORDER BY" + orecieptdetailer;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptdetail = RecieptDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<RecieptDetail> getAll(String orecieptdetailer) {
        ArrayList<RecieptDetail> list = new ArrayList();
        String sql = "SELECT * FROM recieptdetail  ORDER BY" + orecieptdetailer;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RecieptDetail recieptdetail = RecieptDetail.fromRS(rs);
                list.add(recieptdetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public RecieptDetail save(RecieptDetail obj) {

        String sql = "INSERT INTO recieptdetail ( recieptdetail_quantity, recieptdetail_unitprice, recieptdetail_discount, recieptdetail_total, recieptdetail_net, rm_id, reciept_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQuantity());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getDiscount());
            stmt.setFloat(4, obj.getTotal());
            stmt.setFloat(5, obj.getNet());
            stmt.setInt(6, obj.getRmId());
            stmt.setInt(7, obj.getRecieptId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public RecieptDetail update(RecieptDetail obj) {
        String sql = "UPDATE recieptdetail"
                + " SET recieptdetail_quantity =?, recieptdetail_unitprice=?, recieptdetail_discount =?, recieptdetail_total =?, recieptdetail_net =?, rm_id=?, reciept_id=?"
                + " WHERE recieptdetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQuantity());
            stmt.setFloat(2, obj.getUnitPrice());
            stmt.setFloat(3, obj.getDiscount());
            stmt.setFloat(4, obj.getTotal());
            stmt.setFloat(5, obj.getNet());
            stmt.setInt(6, obj.getRmId());
            stmt.setInt(7, obj.getRecieptId());
            stmt.setInt(8, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(RecieptDetail obj) {
        String sql = "DELETE FROM recieptdetail WHERE recieptdetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
