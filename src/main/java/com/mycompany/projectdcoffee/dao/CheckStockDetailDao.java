/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.CheckStockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class CheckStockDetailDao implements Dao<CheckStockDetail>{
    public CheckStockDetail get(int id) {
        CheckStockDetail checkStockDetail = null;
        String sql = "SELECT * FROM check_rawmaterial_detail WHERE crd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockDetail = CheckStockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockDetail;
    }

    public List<CheckStockDetail> getAll() {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_rawmaterial_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckStockDetail> getAll(String where, String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_rawmaterial_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<CheckStockDetail> getAll(String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM check_rawmaterial_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStockDetail save(CheckStockDetail obj) {

        String sql = "INSERT INTO check_rawmaterial_detail(rm_id, csl_id, crm_detail, crm_remanning_material, crm_expired_material, crm_lost_value, crm_date)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRmId());
            stmt.setInt(2, obj.getCslId());
            stmt.setString(3, obj.getMaterialDetail());
            stmt.setInt(4, obj.getRemanningMaterial());
            stmt.setInt(5, obj.getExpiredMaterial());
            stmt.setFloat(6, obj.getLostValue());
            stmt.setString(7, obj.getDate());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStockDetail update(CheckStockDetail obj) {
        String sql = "UPDATE check_rawmaterial_detail" 
                + " SET  rm_id= ?,  csl_id= ?, crm_detail= ?, crm_remanning_material= ?, crm_expired_material= ?, crm_lost_value= ?, crm_date= ?"
                + " WHERE crd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRmId());
            stmt.setInt(2, obj.getCslId());
            stmt.setString(3, obj.getMaterialDetail());
            stmt.setInt(4, obj.getRemanningMaterial());
            stmt.setInt(5, obj.getExpiredMaterial());
            stmt.setFloat(6, obj.getLostValue());
            stmt.setString(7, obj.getDate());
            stmt.setInt(8, obj.getId());
            
            
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStockDetail obj) {
        String sql = "DELETE FROM check_Rawmaterial_Detail WHERE crd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
