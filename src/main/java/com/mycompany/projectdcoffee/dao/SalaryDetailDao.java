/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.dao;

import com.mycompany.projectdcoffee.databaseHelper.DatabaseHelper;
import com.mycompany.projectdcoffee.model.SalaryDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryDetailDao {
    public SalaryDetail get(int id) {
        SalaryDetail salaryDetail = null;
        String sql = "SELECT * FROM salaryDetail WHERE salaryDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                salaryDetail = SalaryDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return salaryDetail;
    }

    public List<SalaryDetail> getAll() {
        ArrayList<SalaryDetail> list = new ArrayList();
        String sql = "SELECT * FROM salaryDetail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryDetail salaryDetail = SalaryDetail.fromRS(rs);
                list.add(salaryDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<SalaryDetail> getAll(String where, String order) {
        ArrayList<SalaryDetail> list = new ArrayList();
        String sql = "SELECT * FROM salaryDetail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryDetail salaryDetail = SalaryDetail.fromRS(rs);
                list.add(salaryDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<SalaryDetail> getAll(String order) {
        ArrayList<SalaryDetail> list = new ArrayList();
        String sql = "SELECT * FROM salaryDetail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                SalaryDetail salaryDetail = SalaryDetail.fromRS(rs);
                list.add(salaryDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public SalaryDetail save(SalaryDetail obj) {

        String sql = "INSERT INTO salaryDetail ( salary_id, emp_id, salaryDetail_shour, salayyDetail_sbase, salaryDetail_date, salary_amount)"
                + "VALUES( ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSdid());
            stmt.setInt(2, obj.getEmpid());
            stmt.setInt(3, obj.getSdshour());
            stmt.setInt(4, obj.getSdbase());
            stmt.setString(5, obj.getSddate());
            stmt.setInt(6, obj.getSsamount());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public SalaryDetail update(SalaryDetail obj) {
        String sql = "UPDATE salaryDetail"
                + " SET salary_id =?, emp_id=?, salaryDetail_shour =?, salaryDetail_sbase =?, salaryDetail_date =?, salary_amount"
                + " WHERE salaryDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSdid());
            stmt.setInt(2, obj.getEmpid());
            stmt.setInt(3, obj.getSdshour());
            stmt.setInt(4, obj.getSdbase());
            stmt.setString(5, obj.getSddate());
            stmt.setInt(6, obj.getSsamount());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(SalaryDetail obj) {
        String sql = "DELETE FROM salaryDetail WHERE salaryDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
