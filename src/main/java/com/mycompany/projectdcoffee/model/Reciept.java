/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import com.mycompany.projectdcoffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Reciept {
    private int id;
    private int employeeId;
    private int branchId;
    private Date orderDate;
    private Date inDate;
    private Date payDate;
    private int materialAddMin;
    private String describe;
    private Employee employee;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList();

    public Reciept(int id, int employeeId, int branchId, Date orderDate, Date inDate, Date payDate, int materialAddMin, String describe) {
        this.id = id;
        this.employeeId = employeeId;
        this.branchId = branchId;
        this.orderDate = orderDate;
        this.inDate = inDate;
        this.payDate = payDate;
        this.materialAddMin = materialAddMin;
        this.describe = describe;
    }
    
    public Reciept( Date orderDate, int employeeId, int branchId, Date inDate, Date payDate, int materialAddMin, String describe) {
        this.id = -1;
        this.employeeId = employeeId;
        this.branchId = branchId;
        this.orderDate = orderDate;
        this.inDate = inDate;
        this.payDate = payDate;
        this.materialAddMin = materialAddMin;
        this.describe = describe;
    }
    
    public Reciept() {
        this.id = -1;
        this.employeeId = 0;
        this.branchId = 0;
        this.orderDate = null;
        this.inDate = null;
        this.payDate = null;
        this.materialAddMin = 0;
        this.describe = "";
    }
    
    public Reciept(int materialAddMin, int employeeId, int branchId, String describe) {
        this.id = -1;
        this.employeeId = 0;
        this.branchId = 0;
        this.orderDate = null;
        this.inDate = null;
        this.payDate = null;
        this.materialAddMin = materialAddMin;
        this.describe = describe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public int getMaterialAddMin() {
        return materialAddMin;
    }

    public void setMaterialAddMin(int materialAddMin) {
        this.materialAddMin = materialAddMin;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList<RecieptDetail> recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public void addRecieptDetail(RecieptDetail recieptDetail){
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }
    
    public void delRecieptDetail(Reciept recieptDetail){
        recieptDetails.remove(recieptDetail);
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", employeeId=" + employeeId + ", branchId=" + branchId + ", orderDate=" + orderDate + ", inDate=" + inDate + ", payDate=" + payDate + ", materialAddMin=" + materialAddMin + ", describe=" + describe + ", employee=" + employee + ", recieptDetails=" + recieptDetails + '}';
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setOrderDate(rs.getTimestamp("reciept_orderdate"));
            reciept.setInDate(rs.getTimestamp("reciept_indate"));
            reciept.setPayDate(rs.getTimestamp("reciept_paydate"));
            reciept.setMaterialAddMin(rs.getInt("reciept_materialaddmin"));
            reciept.setDescribe(rs.getString("reciept_describe"));
            reciept.setBranchId(rs.getInt("branch_id"));
            reciept.setEmployeeId(rs.getInt("employee_id"));
            EmployeeDao employeedao = new EmployeeDao();
            Employee employee = employeedao.get(reciept.getEmployeeId());
            reciept.setEmployee(employee);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
    public void calculateTotal(){
        int totalQty = 0;

        for(RecieptDetail rd: recieptDetails){
            totalQty += rd.getQuantity();
        }
        this.materialAddMin = totalQty;
    }
}
