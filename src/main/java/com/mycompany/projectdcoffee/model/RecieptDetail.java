/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class RecieptDetail {
    int id;
    int rmId;
    int quantity;
    float unitprice;
    float discount;
    float total;
    float net;
    int recieptId;

    public RecieptDetail(int id, int rmId,int quantity, float unitprice, float discount, float total, float net , int recieptIddetial) {
        this.id = id;
        this.rmId = rmId;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.discount = discount;
        this.total = total;
        this.net = net;
        this.recieptId = recieptIddetial;
    }
    
    public RecieptDetail(int quantity, int rmId, float unitprice, float discount, float total, float net, int recieptId) {
        this.id = -1;
        this.rmId = rmId;
        this.quantity = quantity;
        this.unitprice = unitprice;
        this.discount = discount;
        this.total = total;
        this.net = net;
        this.recieptId = recieptId;
    }
    
    public RecieptDetail() {
        this.id = -1;
        this.rmId = 0;
        this.quantity = 0;
        this.unitprice = 0;
        this.discount = 0;
        this.net = 0;
        this.recieptId= 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getUnitPrice() {
        return unitprice;
    }

    public void setUnitprice(float unitprice) {
        this.unitprice = unitprice;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getNet() {
        return net;
    }

    public void setNet(float net) {
        this.net = net;
    }

    public int getRecieptId() {
        return recieptId;
    }

    public void setRecieptId(int recieptId) {
        this.recieptId = recieptId;
    }

    public int getRmId() {
        return rmId;
    }

    public void setRmId(int rmId) {
        this.rmId = rmId;
    }
    
    
   
    
    

    @Override
    public String toString() {
        return "RecieptDetail{" + "id=" + id + ", rm_id=" + rmId + ", quantity=" + quantity + ", unitprice=" + unitprice + ", discount=" + discount + ", total=" + total + ", net=" + net + ", recieptId=" + recieptId + '}';
    }
    
    public static RecieptDetail fromRS(ResultSet rs) {
        RecieptDetail rd = new RecieptDetail();
        try {
            rd.setId(rs.getInt("recieptdetail_id"));
            rd.setRmId(rs.getInt("rm_id"));
            rd.setQuantity(rs.getInt("recieptdetail_quantity"));
            rd.setUnitprice(rs.getFloat("recieptdetail_unitprice"));
            rd.setDiscount(rs.getFloat("recieptdetail_discount"));
            rd.setTotal(rs.getFloat("recieptdetail_total"));
            rd.setNet(rs.getFloat("recieptdetail_net"));
            rd.setRecieptId(rs.getInt("reciept_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return rd;
    }
        
}
