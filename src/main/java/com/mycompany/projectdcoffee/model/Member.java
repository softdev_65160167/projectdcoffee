/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author This PC
 */
public class Member {

    private int id;
    private String name;
    private String tel;
    private int point;


    public Member(int id, String name, String tel, int point) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.point = point;
    }

    public Member(String name, String tel, int point) {
        this.id = -1;
        this.name = name;
        this.tel = tel;
        this.point = point;
    }

    public Member() {
        this.id = -1;
        this.name = "";
        this.tel = "";
        

    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", tel=" + tel + ", point=" + point + '}';
    }

    public static Member fromRS(ResultSet rs) {
        Member menu = new Member();
        try {
            menu.setId(rs.getInt("member_id"));
            menu.setName(rs.getString("member_name"));
            menu.setTel(rs.getString("member_tel"));
            menu.setPoint(rs.getInt("member_point"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return menu;
    }
}
