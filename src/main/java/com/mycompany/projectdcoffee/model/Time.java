/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

/**
 *
 * @author chano
 */

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Time {

    private int id;
    private Timestamp realTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getRealTime() {
        return realTime;
    }

    public void setRealTime(Timestamp realTime) {
        this.realTime = realTime;
    }
    
    @Override
public String toString() {
    return "Time{"
            + "id=" + id
            + ", Real Time=" + realTime
            + '}';
}


    public static Time fromRS(ResultSet rs) {
        Time time = new Time();
        try {
            time.setId(rs.getInt("id"));
            time.setRealTime(rs.getTimestamp("real_time"));
        } catch (SQLException ex) {
            Logger.getLogger(Time.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return time;
    }
}

