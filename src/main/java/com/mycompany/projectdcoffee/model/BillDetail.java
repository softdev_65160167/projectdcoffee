/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uSeR
 */
public class BillDetail {
    private int id;
    private int menuId;
    private String menuName;
    private float menuPrice;
    private int qty;
    private float totalPrice;
    private int billId;

    public BillDetail(int id, int menuId, String menuName, float menuPrice, int qty, float totalPrice, int billId) {
        this.id = id;
        this.menuId = menuId;
        this.menuName = menuName;
        this.menuPrice = menuPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.billId = billId;
    }
    public BillDetail( int menuId, String menuName, float menuPrice, int qty, float totalPrice, int billId) {
        this.id = -1;
        this.menuId = menuId;
        this.menuName = menuName;
        this.menuPrice = menuPrice;
        this.qty = qty;
        this.totalPrice = totalPrice;
        this.billId = billId;
    }
    public BillDetail() {
        this.id = -1;
        this.menuId = 0;
        this.menuName = "";
        this.menuPrice = 0;
        this.qty = 0;
        this.totalPrice = 0;
        this.billId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public float getMenuPrice() {
        return menuPrice;
    }

    public void setMenuPrice(float menuPrice) {
        this.menuPrice = menuPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }
    
    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", menuId=" + menuId + ", menuName=" + menuName + ", menuPrice=" + menuPrice + ", qty=" + qty + ", totalPrice=" + totalPrice + ", billId=" + billId + '}';
    }
   public static BillDetail fromRS(ResultSet rs) {
       BillDetail billDetail = new BillDetail();
    try {
        billDetail.setId(rs.getInt("billDetail_id"));
        billDetail.setMenuId(rs.getInt("menu_id"));
        billDetail.setMenuName(rs.getString("menu_name"));
        billDetail.setMenuPrice(rs.getFloat("menu_price"));
        billDetail.setQty(rs.getInt("qty"));
        billDetail.setTotalPrice(rs.getFloat("total_price"));
        billDetail.setBillId(rs.getInt("bill_id"));
       } catch (SQLException ex) {
         Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
           return null;
     }
       return billDetail;
}
}
