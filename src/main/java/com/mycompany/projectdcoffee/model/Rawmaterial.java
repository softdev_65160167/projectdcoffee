/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uSeR
 */
public class Rawmaterial {
    private int id;
    private String rawName;
    private int rawQoh;
    private float rawPrice;

    public Rawmaterial(int id, String rawName, int rawQoh, float rawPrice) {
        this.id = id;
        this.rawName = rawName;
        this.rawQoh = rawQoh;
        this.rawPrice = rawPrice;
    }
    public Rawmaterial(String rawName, int rawQoh, float rawPrice) {
        this.id = -1;
        this.rawName = rawName;
        this.rawQoh = rawQoh;
        this.rawPrice = rawPrice;
    }
    
   public Rawmaterial() {
        this.id = -1;
        this.rawName = "";
        this.rawQoh = 0;
        this.rawPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRawName() {
        return rawName;
    }

    public void setRawName(String rawName) {
        this.rawName = rawName;
    }

    public int getRawQoh() {
        return rawQoh;
    }

    public void setRawQoh(int rawQoh) {
        this.rawQoh = rawQoh;
    }

    public float getRawPrice() {
        return rawPrice;
    }

    public void setRawPrice(float rawPrice) {
        this.rawPrice = rawPrice;
    }

    @Override
    public String toString() {
        return "Rawmaterial{" + "id=" + id + ", rawName=" + rawName + ", rawQoh=" + rawQoh + ", rawPrice=" + rawPrice + '}';
    }
    
       

  
    public static Rawmaterial fromRS(ResultSet rs) {
        Rawmaterial rawMaterial = new Rawmaterial();
        try {
            rawMaterial.setId(rs.getInt("rm_id"));
            rawMaterial.setRawName(rs.getString("rm_name"));
            rawMaterial.setRawQoh(rs.getInt("rm_qoh"));
            rawMaterial.setRawPrice(rs.getFloat("rm_price"));
        } catch (SQLException ex) {
            Logger.getLogger(Rawmaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return rawMaterial;
    }

    public void addActionListener(ActionListener actionListener) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
