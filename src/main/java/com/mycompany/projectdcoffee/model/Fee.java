/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Fee {
    private int id;
    private String month;
    private Float water;
    private Float electric;
    private Float location;
    private Float total;
    private String status;

    public Fee(int id, String month, Float water, Float electric, Float location, Float total, String status) {
        this.id = id;
        this.month = month;
        this.water = water;
        this.electric = electric;
        this.location = location;
        this.total = total;
        this.status = status;
    }
    
    public Fee( String month, Float water, Float electric, Float location, Float total, String status) {
        this.id = -1;
        this.month = month;
        this.water = water;
        this.electric = electric;
        this.location = location;
        this.total = total;
        this.status = status;
    }
    
    public Fee() {
        this.id = -1;
        this.month = "";
        this.water = 0.0f;
        this.electric = 0.0f;
        this.location = 0.0f;
        this.total = 0.0f;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Float getWater() {
        return water;
    }

    public void setWater(Float water) {
        this.water = water;
    }

    public Float getElectric() {
        return electric;
    }

    public void setElectric(Float electric) {
        this.electric = electric;
    }

    public Float getLocation() {
        return location;
    }

    public void setLocation(Float location) {
        this.location = location;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Fee{" + "id=" + id + ", month=" + month + ", water=" + water + ", electic=" + electric + ", location=" + location + ", total=" + total + ", status=" + status + '}';
    }
    
    public static Fee fromRS(ResultSet rs) {
        Fee fee = new Fee();
        try {
            fee.setId(rs.getInt("fee_id"));
            fee.setMonth(rs.getString("fee_month"));
            fee.setWater(rs.getFloat("fee_water"));
            fee.setElectric(rs.getFloat("fee_electric"));
            fee.setLocation(rs.getFloat("fee_location"));
            fee.setTotal(rs.getFloat("fee_total"));
            fee.setStatus(rs.getString("fee_status"));
        } catch (SQLException ex) {
            Logger.getLogger(Rawmaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return fee;
    }
    
}
