/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class Employee {
    private int id;
    private String bid;
    private String ename;
    private String epassword;
    private String ebirth;
    private String egender;
    private String etel;
    private String eedq;
    private String eposition;
    private String esingin;

    public Employee(int id, String bid, String ename, String epassword, String ebirth, String egender, String etel, String eedq, String eposition, String esingin) {
        this.id = id;
        this.bid = bid;
        this.ename = ename;
        this.epassword = epassword;
        this.ebirth = ebirth;
        this.egender = egender;
        this.etel = etel;
        this.eedq = eedq;
        this.eposition = eposition;
        this.esingin = esingin;
    }

    public Employee(String bid, String ename, String epassword, String ebirth, String egender, String etel, String eedq, String eposition, String esingin) {
        this.id = -1;
        this.bid = bid;
        this.ename = ename;
        this.epassword = epassword;
        this.ebirth = ebirth;
        this.egender = egender;
        this.etel = etel;
        this.eedq = eedq;
        this.eposition = eposition;
        this.esingin = esingin;
    }

    public Employee() {
        this.id = -1;
        this.bid = "";
        this.ename = "";
        this.epassword = "";
        this.ebirth = "";
        this.egender = "";
        this.etel = "";
        this.eedq = "";
        this.eposition = "";
        this.esingin = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getEpassword() {
        return epassword;
    }

    public void setEpassword(String epassword) {
        this.epassword = epassword;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getEbirth() {
        return ebirth;
    }

    public void setEbirth(String ebirth) {
        this.ebirth = ebirth;
    }

    public String getEgender() {
        return egender;
    }

    public void setEgender(String egender) {
        this.egender = egender;
    }

    public String getEtel() {
        return etel;
    }

    public void setEtel(String etel) {
        this.etel = etel;
    }

    public String getEedq() {
        return eedq;
    }

    public void setEedq(String eedq) {
        this.eedq = eedq;
    }

    public String getEposition() {
        return eposition;
    }

    public void setEposition(String eposition) {
        this.eposition = eposition;
    }

    public String getEsingin() {
        return esingin;
    }

    public void setEsingin(String esingin) {
        this.esingin = esingin;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", bid=" + bid + ", ename=" + ename + ", epassword=" + epassword + ", ebirth=" + ebirth + ", egender=" + egender + ", etel=" + etel + ", eedq=" + eedq + ", eposition=" + eposition + ", esingin=" + esingin + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("emp_id"));
            employee.setBid(rs.getString("b_id"));
            employee.setEname(rs.getString("emp_name"));
            employee.setEpassword(rs.getString("emp_password"));
            employee.setEbirth(rs.getString("emp_datebirth"));
            employee.setEgender(rs.getString("emp_genders"));
            employee.setEtel(rs.getString("emp_tel"));
            employee.setEedq(rs.getString("emp_edq"));
            employee.setEposition(rs.getString("emp_position"));
            employee.setEsingin(rs.getString("emp_signin"));

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

}
