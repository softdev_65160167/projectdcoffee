/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uSeR
 */
public class CheckStockDetail {
    private int id;
    private int rmId;
    private int cslId;
    private String materialDetail;
    private int remanningMaterial;
    private int expiredMaterial;
    private float lostValue;
    private String date;

    public CheckStockDetail(int id, int rmId, int cslId, String materialDetail, int remanningMaterial, int expiredMaterial, float lostValue, String date) {
        this.id = id;
        this.rmId = rmId;
        this.cslId = cslId;
        this.materialDetail = materialDetail;
        this.remanningMaterial = remanningMaterial;
        this.expiredMaterial = expiredMaterial;
        this.lostValue = lostValue;
        this.date = date;
    }

    public CheckStockDetail(int rmId, int cslId, String materialDetail, int remanningMaterial, int expiredMaterial, float lostValue, String date) {
        this.id = -1;
        this.rmId = rmId;
        this.cslId = cslId;
        this.materialDetail = materialDetail;
        this.remanningMaterial = remanningMaterial;
        this.expiredMaterial = expiredMaterial;
        this.lostValue = lostValue;
        this.date = date;
    }

    public CheckStockDetail() {
        this.id = -1;
        this.rmId = 0;
        this.cslId = 0;
        this.materialDetail = "";
        this.remanningMaterial = 0;
        this.expiredMaterial = 0;
        this.lostValue = 0;
        this.date = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRmId() {
        return rmId;
    }

    public void setRmId(int rmId) {
        this.rmId = rmId;
    }

    public int getCslId() {
        return cslId;
    }

    public void setCslId(int cslId) {
        this.cslId = cslId;
    }

    public String getMaterialDetail() {
        return materialDetail;
    }

    public void setMaterialDetail(String materialDetail) {
        this.materialDetail = materialDetail;
    }

    public int getRemanningMaterial() {
        return remanningMaterial;
    }

    public void setRemanningMaterial(int remanningMaterial) {
        this.remanningMaterial = remanningMaterial;
    }

    public int getExpiredMaterial() {
        return expiredMaterial;
    }

    public void setExpiredMaterial(int expiredMaterial) {
        this.expiredMaterial = expiredMaterial;
    }

    public float getLostValue() {
        return lostValue;
    }

    public void setLostValue(float lostValue) {
        this.lostValue = lostValue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CheckStockDetail{" + "id=" + id + ", rmId=" + rmId + ", cslId=" + cslId + ", materialDetail=" + materialDetail + ", remanningMaterial=" + remanningMaterial + ", expiredMaterial=" + expiredMaterial + ", lostValue=" + lostValue + ", date=" + date + '}';
    }
    
    public static CheckStockDetail fromRS(ResultSet rs) {
        CheckStockDetail checkStockDetail = new CheckStockDetail();
        try {
            checkStockDetail.setId(rs.getInt("crd_id"));
            checkStockDetail.setRmId(rs.getInt("rm_id"));
            checkStockDetail.setCslId(rs.getInt("csl_id"));
            checkStockDetail.setMaterialDetail(rs.getString("crm_detail"));
            checkStockDetail.setRemanningMaterial(rs.getInt("crm_remanning_material"));
            checkStockDetail.setExpiredMaterial(rs.getInt("crm_expired_material"));
            checkStockDetail.setLostValue(rs.getFloat("crm_lost_value"));
            checkStockDetail.setDate(rs.getString("crm_date"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockDetail;
    }
}
