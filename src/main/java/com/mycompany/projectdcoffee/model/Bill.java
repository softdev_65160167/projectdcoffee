/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
public class Bill {
    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int customerId;
    private int empId;
    private int memberId;
    private int branchId;
    private int scId;
    private int pmId;
    private ArrayList<BillDetail> billDetails = new ArrayList();

    public Bill(int id, Date createdDate, float total, float cash, int totalQty, int customerId, int empId, int memberId, int branchId, int scId, int pmId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.customerId = customerId;
        this.empId = empId;
        this.memberId = memberId;
        this.branchId = branchId;
        this.scId = scId;
        this.pmId = pmId;
    }
    
    public Bill(Date createdDate, float total, float cash, int totalQty, int customerId, int empId, int memberId, int branchId, int scId, int pmId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.customerId = customerId;
        this.empId = empId;
        this.memberId = memberId;
        this.branchId = branchId;
        this.scId = scId;
        this.pmId = pmId;
    }
    public Bill(float total, float cash, int totalQty, int customerId, int empId, int memberId, int branchId, int scId, int pmId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.customerId = customerId;
        this.empId = empId;
        this.memberId = memberId;
        this.branchId = branchId;
        this.scId = scId;
        this.pmId = pmId;
    }
    
    public Bill() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.customerId = 0;
        this.empId = 0;
        this.memberId = 0;
        this.branchId = 0;
        this.scId = 0;
        this.pmId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public int getScId() {
        return scId;
    }

    public void setScId(int scId) {
        this.scId = scId;
    }

    public int getPmId() {
        return pmId;
    }

    public void setPmId(int pmId) {
        this.pmId = pmId;
    }
    
    public ArrayList<BillDetail> getBillDetails() {
        return billDetails;
    }

    public void setBillDetails(ArrayList billDetails) {
        this.billDetails = billDetails;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", customerId=" + customerId + ", empId=" + empId + ", memberId=" + memberId + ", branchId=" + branchId + ", scId=" + scId + ", pmId=" + pmId + ", billDetails=" + billDetails + '}';
    }
    
    public void addBillDetail(BillDetail billDetail){
        billDetails.add(billDetail);
        calculateTotal();
    }
    
    public void addBillDetail(Menu menu,int qty){
        BillDetail rd = new BillDetail(menu.getId(), menu.getName(), menu.getPrice(), qty,qty*menu.getPrice(),id);
        billDetails.add(rd);
        calculateTotal();
    }
    
    public void delBillDetail(BillDetail billDetail){
        billDetails.remove(billDetail);
        calculateTotal();
    }
    
    public void calculateTotal(){
        int totalQty = 0;
        float total = 0.0f;
        for(BillDetail rd: billDetails){
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.total = total;
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("bill_id"));
            bill.setCreatedDate(rs.getTimestamp("created_date"));
            bill.setTotal(rs.getFloat("total"));
            bill.setCash(rs.getFloat("cash"));
            bill.setTotalQty(rs.getInt("bill_totalqty"));
            bill.setCustomerId(rs.getInt("customer_id"));
            bill.setEmpId(rs.getInt("emp_id"));
            bill.setMemberId(rs.getInt("member_id"));
            bill.setBranchId(rs.getInt("b_id"));
            bill.setScId(rs.getInt("sc_id"));
            bill.setPmId(rs.getInt("pm_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }
}
