/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;
import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author uSeR
 */
public class Promotion {
    private int id;
    private Date startdate;
    private Date enddate;
    private String name;
    private String status;
    private ArrayList<PromotionDetail> promotionDetails =new ArrayList();
    

    public Promotion(int id, Date startdate, Date enddate, String name, String status, PromotionDetail promotionDetail) {
        this.id = id;
        this.startdate = startdate;
        this.enddate = enddate;
        this.name = name;
        this.status = status;
    }
  public Promotion(Date startdate, Date enddate, String name, String status, PromotionDetail promotionDetail) {
        this.id = -1;
        this.startdate = startdate;
        this.enddate = enddate;
        this.name = name;
        this.status = status;
    }
     public Promotion() {
        this.id = -1;
        this.startdate = null;
        this.enddate = null;
        this.name = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<PromotionDetail> getPromotionDetail() {
        return promotionDetails;
    }

    public void setPromotionDetails(ArrayList<PromotionDetail> promotionDetails) {
        this.promotionDetails = promotionDetails;
    }
    

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", startdate=" + startdate + ", enddate=" + enddate + ", name=" + name + ", status=" + status + ", promotionDetails=" + promotionDetails + '}';
    }
  

    public void addPromotionDetail(PromotionDetail promotionDetail){
        promotionDetails.add(promotionDetail);
    }
    public void delPromotionDetail(PromotionDetail promotionDetail){
        promotionDetails.remove(promotionDetail);
    }

    
        public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promotion_id"));
            promotion.setStartdate(rs.getDate("promotion_startdate"));
            promotion.setEnddate(rs.getDate("promotion_enddate"));
            promotion.setName(rs.getString("promotion_name"));
            promotion.setStatus(rs.getString("promotion_status"));
            //population

            
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
        
        public int getDiscountForPromotionId(int promotionId) {
            for (PromotionDetail promotionDetail : promotionDetails) {
                if (promotionDetail.getPromotionId() == promotionId) {
                    return promotionDetail.getDiscount();
                }
            }
            return 0;
        }
}
