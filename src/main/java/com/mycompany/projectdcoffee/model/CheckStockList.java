/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uSeR
 */
public class CheckStockList {
    private int id;
    private int empId;
    private int expiredMaterial;
    private int lostMaterial;
    private float lostValue;
    private int totalmaterial;
    private Date dateCheck;

    public CheckStockList(int id, int empId, int expiredMaterial, int lostMaterial, float lostValue, int totalmaterial, Date dateCheck) {
        this.id = id;
        this.empId = empId;
        this.expiredMaterial = expiredMaterial;
        this.lostMaterial = lostMaterial;
        this.lostValue = lostValue;
        this.totalmaterial = totalmaterial;
        this.dateCheck = dateCheck;
    }

    public CheckStockList(int empId, int expiredMaterial, int lostMaterial, float lostValue, int totalmaterial, Date dateCheck) {
        this.id = -1;
        this.empId = empId;
        this.expiredMaterial = expiredMaterial;
        this.lostMaterial = lostMaterial;
        this.lostValue = lostValue;
        this.totalmaterial = totalmaterial;
        this.dateCheck = dateCheck;
    }
    
    public CheckStockList() {
        this.id = -1;
        this.empId = 0;
        this.expiredMaterial = 0;
        this.lostMaterial = 0;
        this.lostValue = 0;
        this.totalmaterial = 0;
        this.dateCheck = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getExpiredMaterial() {
        return expiredMaterial;
    }

    public void setExpiredMaterial(int expiredMaterial) {
        this.expiredMaterial = expiredMaterial;
    }

    public int getLostMaterial() {
        return lostMaterial;
    }

    public void setLostMaterial(int lostMaterial) {
        this.lostMaterial = lostMaterial;
    }

    public float getLostValue() {
        return lostValue;
    }

    public void setLostValue(float lostValue) {
        this.lostValue = lostValue;
    }

    public int getTotalmaterial() {
        return totalmaterial;
    }

    public void setTotalmaterial(int totalmaterial) {
        this.totalmaterial = totalmaterial;
    }

    public Date getDateCheck() {
        return dateCheck;
    }

    public void setDateCheck(Date dateCheck) {
        this.dateCheck = dateCheck;
    }

    @Override
    public String toString() {
        return "CheckStockList{" + "id=" + id + ", empId=" + empId + ", expiredMaterial=" + expiredMaterial + ", lostMaterial=" + lostMaterial + ", lostValue=" + lostValue + ", totalmaterial=" + totalmaterial + ", dateCheck=" + dateCheck + '}';
    }
    
    public static CheckStockList fromRS(ResultSet rs) {
        CheckStockList checkStockList = new CheckStockList();
        try {
            checkStockList.setId(rs.getInt("csl_id"));
            checkStockList.setEmpId(rs.getInt("emp_id"));
            checkStockList.setExpiredMaterial(rs.getInt("csl_expird_material"));
            checkStockList.setLostMaterial(rs.getInt("csl_lost_material"));
            checkStockList.setLostValue(rs.getFloat("csl_lost_value"));
            checkStockList.setTotalmaterial(rs.getInt("csl_totalmaterial"));
            checkStockList.setDateCheck(rs.getTimestamp("csl_date_check"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockList.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockList;
    }
}
