/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author This PC
 */
public class Salary {

    private int id;
    private int empid;
    private int shour;
    private int sbase;
    private int samount;
    private Date sdate;
    private String snote;
    private ArrayList<SalaryDetail>  salaryDetails = new ArrayList();

    public Salary(int id, int empid, int shour, int sbase, int samount, Date sdate, String snote) {
        this.id = id;
        this.empid = empid;
        this.shour = shour;
        this.sbase = sbase;
        this.samount = samount;
        this.sdate = sdate;
        this.snote = snote;
    }

    public Salary(int empid, int shour, int sbase, int samount, Date sdate, String snote) {
        this.id = -1;
        this.empid = empid;
        this.shour = shour;
        this.sbase = sbase;
        this.samount = samount;
        this.sdate = sdate;
        this.snote = snote;
    }

    public Salary() {
        this.id = -1;
        this.empid = 0;
        this.shour = 0;
        this.sbase = 0;
        this.samount = 0;
        this.sdate = null;
        this.snote = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public int getShour() {
        return shour;
    }

    public void setShour(int shour) {
        this.shour = shour;
    }

    public int getSbase() {
        return sbase;
    }

    public void setSbase(int sbase) {
        this.sbase = sbase;
    }

    public int getSamount() {
        return samount;
    }

    public void setSamount(int samount) {
        this.samount = samount;
    }

    public Date getSdate() {
        return sdate;
    }

    public void setSdate(Date sdate) {
        this.sdate = sdate;
    }

    public String getSnote() {
        return snote;
    }

    public void setSnote(String snote) {
        this.snote = snote;
    }

    public void addSalaryDetail(SalaryDetail salaryDetail) {
        salaryDetails.add(salaryDetail);
    }

//    public void addSalaryDetail(Product product, int qty) {
//        SalaryDetail rd = new SalaryDetail(product.getId(), product.getName(),
//                 product.getPrice(), qty, qty * product.getPrice(), -1);
//        salaryDetails.add(rd);
//
//    }

    public void delSalaryDetail(SalaryDetail receptDetail) {
        salaryDetails.remove(receptDetail);
    }

    @Override
    public String toString() {
        return "Saraly{" + "id=" + id + ", empid=" + empid + ", shour=" + shour + ", sbase=" + sbase + ", samount=" + samount + ", sdate=" + sdate + ", snote=" + snote + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("salary_slipid"));
            salary.setEmpid(rs.getInt("emp_id"));
            salary.setShour(rs.getInt("salary_hour"));
            salary.setSbase(rs.getInt("salary_base"));
            salary.setSamount(rs.getInt("salary_amount"));
            salary.setSdate(rs.getDate("salary_date"));
            salary.setSnote(rs.getString("salary_note"));
        } catch (SQLException ex) {
            Logger.getLogger(Salary.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }
}
