/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BillReport {
    private int id;
    private String name;
    private int totalQuantity;
    private float totalPrice;

    public BillReport(int id, String name, int totalQuantity, float totalPrice) {
        this.id = id;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }
    
     public BillReport(String name, int totalQuantity, float totalPrice) {
        this.id = -1;
        this.name = name;
        this.totalQuantity = totalQuantity;
        this.totalPrice = totalPrice;
    }
     
     public BillReport() {
        this.id = -1;
        this.name = "";
        this.totalQuantity = 0;
        this.totalPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(int totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "BillReport{" + "id=" + id + ", name=" + name + ", totalQuantity=" + totalQuantity + ", totalPrice=" + totalPrice + '}';
    }
     
     public static BillReport fromRS(ResultSet rs) {
        BillReport obj = new BillReport();
        try {
            obj.setId(rs.getInt("menu_id"));
            obj.setName(rs.getString("menu_name"));
            obj.setTotalQuantity(rs.getInt("total_qty_ordered"));
            obj.setTotalPrice(rs.getFloat("total"));
        } catch (SQLException ex) {
            Logger.getLogger(BillReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    }
}
