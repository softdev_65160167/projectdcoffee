/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author This PC
 */
public class SalaryDetail {

    private int id;
    private int sdid;
    private int empid;
    private int sdshour;
    private int sdbase;
    private String sddate;
    private int ssamount;
    private ArrayList<SalaryDetail>  SalaryDetails = new ArrayList();

    public SalaryDetail(int id, int sdid, int empid, int sdshour, int sdbase, String sddate, int ssamount) {
        this.id = id;
        this.sdid = sdid;
        this.empid = empid;
        this.sdshour = sdshour;
        this.sdbase = sdbase;
        this.sddate = sddate;
        this.ssamount = ssamount;
    }
    
    public SalaryDetail( int sdid, int empid, int sdshour, int sdbase, String sddate, int ssamount) {
        this.id = -1;
        this.sdid = sdid;
        this.empid = empid;
        this.sdshour = sdshour;
        this.sdbase = sdbase;
        this.sddate = sddate;
        this.ssamount = ssamount;
    }
    public SalaryDetail(int sdid, int empid, int sdshour, int sdbase, int ssamount) {
        this.sdid = sdid;
        this.empid = empid;
        this.sdshour = sdshour;
        this.sdbase = sdbase;
        this.ssamount = ssamount;
    }
    
    public SalaryDetail() {
        this.id = 0;
        this.sdid = 0;
        this.empid = 0;
        this.sdshour = 0;
        this.sdbase = 0;
        this.sddate = "";
        this.ssamount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSdid() {
        return sdid;
    }

    public void setSdid(int sdid) {
        this.sdid = sdid;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public int getSdshour() {
        return sdshour;
    }

    public void setSdshour(int sdshour) {
        this.sdshour = sdshour;
    }

    public int getSdbase() {
        return sdbase;
    }

    public void setSdbase(int sdbase) {
        this.sdbase = sdbase;
    }

    public String getSddate() {
        return sddate;
    }

    public void setSddate(String sddate) {
        this.sddate = sddate;
    }

    public int getSsamount() {
        return ssamount;
    }

    public void setSsamount(int ssamount) {
        this.ssamount = ssamount;
    }
    public ArrayList<SalaryDetail> getSalaryDetails() {
        return SalaryDetails;
    }
        public void addSalaryDetail(SalaryDetail salaryDetail) {
        SalaryDetails.add(salaryDetail);
    }

    @Override
    public String toString() {
        return "SalaryDetail{" + "id=" + id + ", sdid=" + sdid + ", empid=" + empid + ", sdshour=" + sdshour + ", sdbase=" + sdbase + ", sddate=" + sddate + ", ssamount=" + ssamount + '}';
    }
    


    public static SalaryDetail fromRS(ResultSet rs) {
        SalaryDetail salarydetail = new SalaryDetail();
        try {
            salarydetail.setId(rs.getInt("salaryDetail_id"));
            salarydetail.setSdid(rs.getInt("salary_id"));
            salarydetail.setEmpid(rs.getInt("emp_id"));
            salarydetail.setSdshour(rs.getInt("salayyDetail_sbase"));
            salarydetail.setSddate(rs.getString("salaryDetail_date"));
            salarydetail.setSsamount(rs.getInt("salary_amount"));
        } catch (SQLException ex) {
            Logger.getLogger(SalaryDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salarydetail;
    }

    
}
