/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class SellingChannel {
    private int id;
    private String type;

    public SellingChannel(int id, String type) {
        this.id = id;
        this.type = type;
    }
    
    public SellingChannel( String type) {
        this.id = -1;
        this.type = type;
    }
    
    public SellingChannel() {
        this.id = -1;
        this.type = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "SellingChannel{" + "id=" + id + ", type=" + type + '}';
    }
    
    public static SellingChannel fromRS(ResultSet rs) {
        SellingChannel sc = new SellingChannel();
        try {
            sc.setId(rs.getInt("sc_id"));
            sc.setType(rs.getString("sc_type"));
        } catch (SQLException ex) {
            Logger.getLogger(Rawmaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return sc;
    }
}
