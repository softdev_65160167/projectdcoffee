/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chano
 */
public class WorkInOut {

    public static Date parseDate(String dateString) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    int id;
    int empId;
    Time timeIn;
    Time timeOut;
    Date date;

    public WorkInOut(int id, int empId, Time timeIn, Time timeOut, Date date) {
        this.id = id;
        this.empId = empId;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.date = date;
    }

    public WorkInOut(int empId, Time timeIn, Time timeOut, Date date) {
        this.id = -1;
        this.empId = empId;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.date = date;
    }

    public WorkInOut() {
        this.id = -1;
        this.empId = 0;
        this.timeIn = Time.valueOf("08:00:00");
        this.timeOut = Time.valueOf("16:00:00");
        this.date = new Date();
    }

    public int getId() {
        return id;
    }

    public int getEmpId() {
        return empId;
    }

    public Time getTimeIn() {
        return timeIn;
    }

    public Time getTimeOut() {
        return timeOut;
    }

    public Date getDate() {
        return date;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public void setTimeIn(Time timeIn) {
        this.timeIn = timeIn;
    }

    public void setTimeOut(Time timeOut) {
        this.timeOut = timeOut;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "WorkInOut{"
                + "id=" + id
                + ", empId=" + empId
                + ", timeIn=" + timeIn
                + ", timeOut=" + timeOut
                + ", date=" + formatDate(date)
                + '}';
    }
    

    
    /*public static WorkInOut fromRS(ResultSet rs) {
        WorkInOut workinout = new WorkInOut();
        try {
            workinout.setId(rs.getInt("wio_id"));
            workinout.setEmpId(rs.getInt("emp_id"));
            //workinout.setTimeIn(rs.getTime("wio_in"));
            //workinout.setTimeOut(rs.getTime("wio_out"));
            //workinout.setDate(rs.getDate("wio_date"));
            String timeInStr = rs.getString("wio_in");
            String timeOutStr = rs.getString("wio_out");
            workinout.setTimeIn(Time.valueOf(timeInStr));
            workinout.setTimeOut(Time.valueOf(timeOutStr));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = rs.getString("wio_date");
            workinout.setDate(parseDate(dateStr));
        } catch (SQLException ex) {
            Logger.getLogger(WorkInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workinout;
    }*/

    public static WorkInOut fromRS(ResultSet rs) {
        WorkInOut workinout = new WorkInOut();
        try {
            workinout.setId(rs.getInt("wio_id"));
            workinout.setEmpId(rs.getInt("emp_id"));
            
            String timeInStr = rs.getString("wio_in");
            String timeOutStr = rs.getString("wio_out");
            Time timeIn = null;
            Time timeOut = null;
            try {
                if (timeInStr != null) {
                    timeIn = Time.valueOf(timeInStr);
                }
                if (timeOutStr != null) {
                    timeOut = Time.valueOf(timeOutStr);
                }
            } catch (IllegalArgumentException e) {
                // ในกรณีที่ข้อมูลเวลาไม่สามารถแปลงได้
                // คุณสามารถจัดการข้อผิดพลาดที่นี่ หรือทำการรายงานข้อผิดพลาดต่อไป
                e.printStackTrace();
            }

            workinout.setTimeIn(timeIn);
            workinout.setTimeOut(timeOut);
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateStr = rs.getString("wio_date");
            workinout.setDate(parseDate(dateStr));
        } catch (SQLException ex) {
            Logger.getLogger(WorkInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return workinout;
    }

}
