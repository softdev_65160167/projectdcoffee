/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author uSeR
 */
public class PromotionDetail {
    private int id;
    private int promotionId;
    private String condition;
    private int discount;

    public PromotionDetail(int id, int promotionId, String condition, int discount) {
        this.id = id;
        this.promotionId = promotionId;
        this.condition = condition;
        this.discount = discount;
    }
    public PromotionDetail(int promotionId, String condition, int discount) {
        this.id = -1;
        this.promotionId = promotionId;
        this.condition = condition;
        this.discount = discount;
    }
    public PromotionDetail() {
        this.id = -1;
        this.promotionId = 0;
        this.condition = "";
        this.discount = 0;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getPromotionId() {
        return promotionId;
    }
    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }
    public String getCondition() {
        return condition;
    }
    public void setCondition(String condition) {
        this.condition = condition;
    }
    public int getDiscount() {
        return discount;
    }
    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "PromotionDetail{" + "id=" + id + ", promotionId=" + promotionId + ", condition=" + condition + ", discount=" + discount + '}';
    }
    
    public static PromotionDetail fromRS(ResultSet rs) {
        PromotionDetail promotionDetail = new PromotionDetail();
        try {
            promotionDetail.setId(rs.getInt("promotionDetail_id"));
            promotionDetail.setPromotionId(rs.getInt("promotion_id"));
            promotionDetail.setCondition(rs.getString("promotionDetail_condition"));
            promotionDetail.setDiscount(rs.getInt("promotionDetail_discount"));
        } catch (SQLException ex) {
            Logger.getLogger(PromotionDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotionDetail;
    }
}
