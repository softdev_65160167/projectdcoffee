/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class SalaryReport {
    private int id;
    private int srdid;
    private int empid;
    private String srdate;
    private int sramount;

    public SalaryReport(int id, int srdid, int empid, String srdate, int sramount) {
        this.id = id;
        this.srdid = srdid;
        this.empid = empid;
        this.srdate = srdate;
        this.sramount = sramount;
    }
    
    public SalaryReport( int srdid, int empid, String srdate, int sramount) {
        this.id = -1;
        this.srdid = srdid;
        this.empid = empid;
        this.srdate = srdate;
        this.sramount = sramount;
    }
    
    public SalaryReport() {
        this.id = 0;
        this.srdid = 0;
        this.empid = 0;
        this.srdate = "";
        this.sramount = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSrdid() {
        return srdid;
    }

    public void setSrdid(int srdid) {
        this.srdid = srdid;
    }

    public int getEmpid() {
        return empid;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public String getSrdate() {
        return srdate;
    }

    public void setSrdate(String srdate) {
        this.srdate = srdate;
    }

    public int getSramount() {
        return sramount;
    }

    public void setSramount(int sramount) {
        this.sramount = sramount;
    }

    @Override
    public String toString() {
        return "SalaryReport{" + "id=" + id + ", srdid=" + srdid + ", empid=" + empid + ", srdate=" + srdate + ", sramount=" + sramount + '}';
    }
    
    public static SalaryReport fromRS(ResultSet rs) {
        SalaryReport salaryreport = new SalaryReport();
        try {
            salaryreport.setId(rs.getInt("salaryReport_id"));
            salaryreport.setSrdid(rs.getInt("salaryDetail_id"));
            salaryreport.setEmpid(rs.getInt("emp_id"));
            salaryreport.setSrdate(rs.getString("salaryReport_date"));
            salaryreport.setSramount(rs.getInt("salaryReport_amount"));
        } catch (SQLException ex) {
            Logger.getLogger(SalaryDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salaryreport;
    }
    
    
    
    
}
