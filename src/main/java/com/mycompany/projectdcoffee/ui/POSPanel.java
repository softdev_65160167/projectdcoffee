/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;

import com.mycompany.projectdcoffee.component.BakeryListPanel;
import com.mycompany.projectdcoffee.component.BuyMenutable;
import com.mycompany.projectdcoffee.component.MenuListPanel;
import com.mycompany.projectdcoffee.model.Bill;
import com.mycompany.projectdcoffee.model.BillDetail;
import com.mycompany.projectdcoffee.model.Customer;
import com.mycompany.projectdcoffee.model.Employee;
import com.mycompany.projectdcoffee.model.Member;
import com.mycompany.projectdcoffee.model.Menu;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.service.BillService;
import com.mycompany.projectdcoffee.service.CustomerService;
import com.mycompany.projectdcoffee.service.EmployeeService;
import com.mycompany.projectdcoffee.service.MemberService;
import com.mycompany.projectdcoffee.service.PromotionService;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ADMIN
 */
public class POSPanel extends javax.swing.JPanel implements BuyMenutable {
    private String path = "1";
    BillService billService = new BillService();
    Bill bill;
    Member mem;
    MemberService ms = new MemberService();
    Promotion pro;
    PromotionService ps = new PromotionService();
    int earnPoint;
    float usedPoint;
    float discount = 0.0f;
    float net;
    int proid;
    private final MenuListPanel menuListPanel;
    private final BakeryListPanel bakeryListPanel;
    private Member editedMember;
    MainMenuEmployee mmemp;
    String empName;
    Employee emp;
    EmployeeService es = new EmployeeService();
    

    /**
     * Creates new form MenuPanel
     */
    public POSPanel() {
        initComponents();
        bill = new Bill();
        menuListPanel = new MenuListPanel();
        bakeryListPanel = new BakeryListPanel();
        menuListPanel.addOnBuyMenu(this);
        bakeryListPanel.addOnBuyMenu(this);
        scrMenuList.setViewportView(menuListPanel);
        tblBillDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return bill.getBillDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return billDetail.getMenuName();
                    case 1:
                        return billDetail.getMenuPrice();
                    case 2:
                        return billDetail.getQty();
                    case 3:
                        return billDetail.getTotalPrice();
                    default:
                        return "";
                }
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    billDetail.setQty(qty);
                    bill.calculateTotal();
                    refreshBill();
                }

            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }
        });
        enableForm(false);
    }

    private void refreshBill() {
        tblBillDetail.revalidate();
        tblBillDetail.repaint();
        earnPoint = (int) (bill.getTotal() / 10);
        lbldisTU.setText(bill.getTotalQty() + " Item");
        lbldisT.setText(bill.getTotal() + " Baht");
        lbldisD.setText(discount + " Baht");
        lbldisNP.setText(bill.getTotal() - discount + " Baht");
        if (rdioMem.isSelected()) {
            lbldisEarnPoint.setText(earnPoint + " Point");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SweetLevel_btnGroup = new javax.swing.ButtonGroup();
        Size_btnGroup = new javax.swing.ButtonGroup();
        Type_btnGroup = new javax.swing.ButtonGroup();
        Header = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblBillDetail = new javax.swing.JTable();
        scrMenuList = new javax.swing.JScrollPane();
        jLabel1 = new javax.swing.JLabel();
        drinkbtn = new javax.swing.JButton();
        bekerybtn = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        netbtn = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblTotalUnit = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lbldisTU = new javax.swing.JLabel();
        lbldisNP = new javax.swing.JLabel();
        lbldisEarnPoint = new javax.swing.JLabel();
        lbldisD = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        txtCash = new javax.swing.JTextField();
        lbldisTU1 = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        lbldisChange = new javax.swing.JLabel();
        lblChange1 = new javax.swing.JLabel();
        lbldisPS = new javax.swing.JLabel();
        lblTotalUnit1 = new javax.swing.JLabel();
        lbldisMemName = new javax.swing.JLabel();
        lblTotalUnit2 = new javax.swing.JLabel();
        lblTotalUnit3 = new javax.swing.JLabel();
        lblTotalUnit4 = new javax.swing.JLabel();
        lblTotalUnit5 = new javax.swing.JLabel();
        lblTotalUnit6 = new javax.swing.JLabel();
        lbldisT = new javax.swing.JLabel();
        lbldismemPoint = new javax.swing.JLabel();
        lbldisEarnPoint1 = new javax.swing.JLabel();
        lbldisRemainingPoint = new javax.swing.JLabel();
        txtPoint = new javax.swing.JTextField();
        lblTotalUnit7 = new javax.swing.JLabel();
        lbldisPromotionname = new javax.swing.JLabel();
        txtTel = new javax.swing.JTextField();
        promotionbtn = new javax.swing.JButton();
        rdioMem = new javax.swing.JRadioButton();
        rdioPromotion = new javax.swing.JRadioButton();
        lblTotalUnit8 = new javax.swing.JLabel();
        txtPromotion = new javax.swing.JTextField();
        usepromotionbtn = new javax.swing.JButton();
        lblTotalUnit9 = new javax.swing.JLabel();
        txtCusName = new javax.swing.JTextField();
        lblTotalUnit10 = new javax.swing.JLabel();
        txtEmpName = new javax.swing.JTextField();
        regismembtn = new javax.swing.JButton();
        confirmbtn = new javax.swing.JButton();
        btnCalculate = new javax.swing.JButton();
        clearbtn = new javax.swing.JButton();
        serachmembtn1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(231, 198, 152));
        setPreferredSize(new java.awt.Dimension(1400, 800));

        Header.setBackground(new java.awt.Color(255, 255, 255));
        Header.setFont(new java.awt.Font("Segoe UI", 1, 50)); // NOI18N
        Header.setText("Menu Order");

        tblBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblBillDetail);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Product List");

        drinkbtn.setText("Drink");
        drinkbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drinkbtnActionPerformed(evt);
            }
        });

        bekerybtn.setText("Bakery");
        bekerybtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bekerybtnActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(231, 198, 152));

        netbtn.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        netbtn.setForeground(new java.awt.Color(0, 0, 0));
        netbtn.setText("Net Price :");

        lblTotal.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblTotal.setText("Total :");

        lblTotalUnit.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit.setText("Total Unit :");

        lblDiscount.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(0, 0, 0));
        lblDiscount.setText("Discount :");

        lbldisTU.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisTU.setForeground(new java.awt.Color(0, 0, 0));
        lbldisTU.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisTU.setText("-   Item");

        lbldisNP.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisNP.setForeground(new java.awt.Color(0, 0, 0));
        lbldisNP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisNP.setText("-   Baht");

        lbldisEarnPoint.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisEarnPoint.setForeground(new java.awt.Color(0, 0, 0));
        lbldisEarnPoint.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisEarnPoint.setText("-   Point");

        lbldisD.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisD.setForeground(new java.awt.Color(0, 0, 0));
        lbldisD.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisD.setText("-   Baht");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Select Payment", "Cash", "Credit Card", "QrCode" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        txtCash.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCash.setText("0.00");
        txtCash.setToolTipText("");
        txtCash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCashActionPerformed(evt);
            }
        });

        lbldisTU1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisTU1.setForeground(new java.awt.Color(0, 0, 0));
        lbldisTU1.setText("Baht");

        lblChange.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblChange.setForeground(new java.awt.Color(0, 0, 0));
        lblChange.setText("Change :");

        lbldisChange.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisChange.setForeground(new java.awt.Color(0, 0, 0));
        lbldisChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisChange.setText("-   Baht");

        lblChange1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblChange1.setForeground(new java.awt.Color(0, 0, 0));
        lblChange1.setText("Payment Status");

        lbldisPS.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisPS.setForeground(new java.awt.Color(0, 0, 0));
        lbldisPS.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbldisPS.setText("---");

        lblTotalUnit1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit1.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit1.setText("Member Name :");

        lbldisMemName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisMemName.setForeground(new java.awt.Color(0, 0, 0));
        lbldisMemName.setText("-");

        lblTotalUnit2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit2.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit2.setText("Member Tel :");

        lblTotalUnit3.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit3.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit3.setText("Member Point :");

        lblTotalUnit4.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit4.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit4.setText("Remaining Point :");

        lblTotalUnit5.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit5.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit5.setText("Used Point :");

        lblTotalUnit6.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit6.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit6.setText("Earn Point :");

        lbldisT.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisT.setForeground(new java.awt.Color(0, 0, 0));
        lbldisT.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldisT.setText("-   Baht");

        lbldismemPoint.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldismemPoint.setForeground(new java.awt.Color(0, 0, 0));
        lbldismemPoint.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbldismemPoint.setText("-   Point");

        lbldisEarnPoint1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisEarnPoint1.setForeground(new java.awt.Color(0, 0, 0));
        lbldisEarnPoint1.setText("Point");

        lbldisRemainingPoint.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisRemainingPoint.setForeground(new java.awt.Color(0, 0, 0));
        lbldisRemainingPoint.setText("-   Point");

        txtPoint.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPoint.setText("0");
        txtPoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPointActionPerformed(evt);
            }
        });

        lblTotalUnit7.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit7.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit7.setText("Promotion Name :");

        lbldisPromotionname.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lbldisPromotionname.setForeground(new java.awt.Color(0, 0, 0));
        lbldisPromotionname.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbldisPromotionname.setText("- ");

        txtTel.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTel.setText("XXX-XXX-XXXX");
        txtTel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelActionPerformed(evt);
            }
        });

        promotionbtn.setText("Promotion");
        promotionbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                promotionbtnActionPerformed(evt);
            }
        });

        rdioMem.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        rdioMem.setForeground(new java.awt.Color(0, 0, 0));
        rdioMem.setText("Member");
        rdioMem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdioMemActionPerformed(evt);
            }
        });

        rdioPromotion.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        rdioPromotion.setForeground(new java.awt.Color(0, 0, 0));
        rdioPromotion.setText("Used Promotion");
        rdioPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdioPromotionActionPerformed(evt);
            }
        });

        lblTotalUnit8.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit8.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit8.setText("Promotion ID :");

        txtPromotion.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPromotion.setText("0");
        txtPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPromotionActionPerformed(evt);
            }
        });

        usepromotionbtn.setText("Use Promotion");
        usepromotionbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usepromotionbtnActionPerformed(evt);
            }
        });

        lblTotalUnit9.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit9.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit9.setText("Customer Name :");

        txtCusName.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCusName.setText("Null");
        txtCusName.setToolTipText("");
        txtCusName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCusNameActionPerformed(evt);
            }
        });

        lblTotalUnit10.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblTotalUnit10.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalUnit10.setText("Employee Name :");

        txtEmpName.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtEmpName.setToolTipText("");
        txtEmpName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmpNameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblTotal)
                                .addComponent(lblDiscount)
                                .addComponent(netbtn)
                                .addComponent(lblTotalUnit))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(110, 110, 110)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lbldisTU, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lbldisT, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(98, 98, 98)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(lbldisD, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lbldisNP, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblTotalUnit4)
                                .addGap(108, 108, 108)
                                .addComponent(lbldisRemainingPoint))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblTotalUnit1)
                                .addGap(18, 18, 18)
                                .addComponent(lbldisMemName, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTotalUnit5)
                                    .addComponent(lblTotalUnit2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtTel)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lbldisEarnPoint1))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTotalUnit6)
                                    .addComponent(lblTotalUnit3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lbldismemPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                    .addComponent(lbldisEarnPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblTotalUnit7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbldisPromotionname, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(81, 81, 81)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rdioMem)
                                    .addComponent(rdioPromotion))
                                .addGap(33, 33, 33)
                                .addComponent(promotionbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(lbldisPS, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCash)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lbldisTU1))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblChange)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lbldisChange, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(lblChange1)
                                        .addGap(61, 61, 61))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblTotalUnit9)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtCusName, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblTotalUnit10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(0, 9, Short.MAX_VALUE)))
                                .addContainerGap())))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotalUnit8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(usepromotionbtn)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(rdioMem))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblTotalUnit1)
                                .addComponent(lbldisMemName)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblTotalUnit2)
                                .addComponent(txtTel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(rdioPromotion)))
                    .addComponent(promotionbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit3)
                            .addComponent(lbldismemPoint))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit6)
                            .addComponent(lbldisEarnPoint))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit5)
                            .addComponent(lbldisEarnPoint1)
                            .addComponent(txtPoint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit4)
                            .addComponent(lbldisRemainingPoint))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTotalUnit8)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(usepromotionbtn)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit9)
                            .addComponent(txtCusName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit10)
                            .addComponent(txtEmpName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit7)
                            .addComponent(lbldisPromotionname))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotalUnit)
                            .addComponent(lbldisTU))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTotal)
                            .addComponent(lbldisT))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblDiscount)
                            .addComponent(lbldisD))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(netbtn)
                            .addComponent(lbldisNP)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCash, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbldisTU1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblChange)
                            .addComponent(lbldisChange))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblChange1)
                        .addGap(17, 17, 17)
                        .addComponent(lbldisPS)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        regismembtn.setText("Register Member");
        regismembtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                regismembtnActionPerformed(evt);
            }
        });

        confirmbtn.setText("Confirm");
        confirmbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmbtnActionPerformed(evt);
            }
        });

        btnCalculate.setBackground(new java.awt.Color(51, 204, 0));
        btnCalculate.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        clearbtn.setBackground(new java.awt.Color(255, 0, 51));
        clearbtn.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        clearbtn.setText("Clear Order");
        clearbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearbtnActionPerformed(evt);
            }
        });

        serachmembtn1.setText("Search Member");
        serachmembtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                serachmembtn1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(drinkbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(bekerybtn, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Header)
                        .addGap(191, 191, 191))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(scrMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, 801, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 753, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(56, 56, 56))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(regismembtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(serachmembtn1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(clearbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(confirmbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Header)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(drinkbtn, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                            .addComponent(bekerybtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(regismembtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(confirmbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(clearbtn, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(serachmembtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(scrMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, 721, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        float cash = Float.parseFloat(txtCash.getText());
        bill.setCash(cash);
        net = bill.getTotal() - discount;
        float change = (cash - net);
        lbldisChange.setText(change + " Baht");
        refreshBill();
        lbldisPS.setText("Waiting for payment processing...");
        openDialogQr();
    }//GEN-LAST:event_btnCalculateActionPerformed

    private void openDialogQr() {
        if(jComboBox1.getSelectedIndex() == 3){
            JFrame frame = (JFrame) SwingUtilities.getRoot(this);
            qrCodePayDialog qrDialog = new qrCodePayDialog(frame, path);
            qrDialog.setLocationRelativeTo(this);
            qrDialog.setVisible(true);
            qrDialog.addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosed(WindowEvent e) {
                    
                }
            });
        }
    }

    private void setMemberCusName() {
        txtCusName.setText(mem.getName());
    }

    private void clearBill() {
        txtEmpName.setText("");
        net = 0;
        discount = 0;
        txtTel.setText("XXX-XXX-XXXX");
        lbldisMemName.setText("-");
        lbldismemPoint.setText("- Point");
        lbldisEarnPoint.setText("- Point");
        txtCash.setText("0.00");
        lbldisChange.setText("- Baht");
        lbldisPS.setText("---");
        lbldisD.setText("-");
        txtPromotion.setText("0");
        rdioMem.setSelected(false);
        rdioPromotion.setSelected(false);
        lbldisD.setText("- Baht");
        lbldisNP.setText("- Baht");
        lbldisRemainingPoint.setText("- Point");
        lbldisPromotionname.setText("-");
        txtPoint.setText("0");
        txtCusName.setText("Enter Name");
        bill = new Bill();
        refreshBill();
    }
    private void clearbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearbtnActionPerformed
        clearBill();
    }//GEN-LAST:event_clearbtnActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        int type = jComboBox1.getSelectedIndex();
        if (type == 0) {
            JOptionPane.showMessageDialog(null, "Select Payment Method", "Alert", JOptionPane.ERROR_MESSAGE);
        }
        if (type == 1) {
            bill.setPmId(1);
        }
        if (type == 2) {
            bill.setPmId(3);
        }
        if (type == 3) {
            bill.setPmId(2);
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void txtCashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCashActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCashActionPerformed

    private void txtTelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelActionPerformed

    }//GEN-LAST:event_txtTelActionPerformed

    private void serachmembtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_serachmembtn1ActionPerformed
        if (rdioMem.isSelected()) {
            String tel = txtTel.getText();
            mem = ms.getByTel(tel);
            lbldisMemName.setText(mem.getName());
            lbldismemPoint.setText(mem.getPoint() + " Point");
            setMemberCusName();
        }
    }//GEN-LAST:event_serachmembtn1ActionPerformed

    private void regismembtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_regismembtnActionPerformed
        editedMember = new Member();
        openDialog();
    }//GEN-LAST:event_regismembtnActionPerformed

    private void promotionbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_promotionbtnActionPerformed
        newPromotionPanel me = new newPromotionPanel();
        me.setLocationRelativeTo(this);
        me.setVisible(true);
    }//GEN-LAST:event_promotionbtnActionPerformed

    private void confirmbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmbtnActionPerformed
        int type = jComboBox1.getSelectedIndex();
        if (type == 0 ) {
            JOptionPane.showMessageDialog(null, "Select Payment Method", "Alert", JOptionPane.ERROR_MESSAGE);
        }else if(txtEmpName.getText().equals("")){
            JOptionPane.showMessageDialog(null, "Enter Employee Name", "Alert", JOptionPane.ERROR_MESSAGE);
        }else{
            Customer cus = addCustomerName();
            if(rdioMem.isSelected()){
                bill.setMemberId(mem.getId());
            }
            bill.setCustomerId(cus.getId());
            bill.setBranchId(1);
            bill.setScId(1);
            setEmpId();
            bill.setEmpId(emp.getId());
            System.out.println("" + bill);
            billService.addNew(bill);
            CheckMember();
            openInvoiceDialog();
            clearBill();
        }
    }//GEN-LAST:event_confirmbtnActionPerformed

    private void setEmpId() {
        empName = txtEmpName.getText();
        emp = es.getByName(empName);
    }

    private Customer addCustomerName() {
        CustomerService cs = new CustomerService();
        Customer cus = new Customer(txtCusName.getText());
        cs.addNew(cus);
        return cus;
    }

    private void rdioMemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdioMemActionPerformed
        enableForm(true);
        refreshBill();
    }//GEN-LAST:event_rdioMemActionPerformed

    private void bekerybtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bekerybtnActionPerformed
        scrMenuList.setViewportView(bakeryListPanel);
    }//GEN-LAST:event_bekerybtnActionPerformed

    private void drinkbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drinkbtnActionPerformed
        scrMenuList.setViewportView(menuListPanel);
    }//GEN-LAST:event_drinkbtnActionPerformed

    private void rdioPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdioPromotionActionPerformed
        enableFormProOnly(true);
        refreshBill();
    }//GEN-LAST:event_rdioPromotionActionPerformed

    private void usepromotionbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usepromotionbtnActionPerformed
        if (rdioPromotion.isSelected()) {
            proid = Integer.parseInt(txtPromotion.getText());
            pro = ps.getByid(proid);
            if (pro.getStatus().equals("Attive")) {
                lbldisPromotionname.setText(pro.getName());
                discount += pro.getDiscountForPromotionId(proid);
                lbldisD.setText(discount + " Baht");
                refreshBill();
            } else {
                JOptionPane.showMessageDialog(null, "Promotion Expried", "Alert", JOptionPane.ERROR_MESSAGE);
                rdioPromotion.setSelected(false);
                lbldisPromotionname.setText("-");
                txtPromotion.setText("0");
            }
        }
    }//GEN-LAST:event_usepromotionbtnActionPerformed

    private void txtPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPromotionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPromotionActionPerformed

    private void txtCusNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCusNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCusNameActionPerformed

    private void txtPointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPointActionPerformed
        usedPoint = Float.parseFloat(txtPoint.getText());
        discount += usedPoint;
        if (usedPoint > mem.getPoint() || mem.getPoint() == 0) {
                JOptionPane.showMessageDialog(null, "Not enough points", "Alert", JOptionPane.ERROR_MESSAGE);
            }
        refreshBill();
    }//GEN-LAST:event_txtPointActionPerformed

    private void txtEmpNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmpNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmpNameActionPerformed

    private void enableForm(boolean status) {
        if (status == false) {
            txtTel.setText("XXX-XXX-XXXX");
            txtPoint.setText("0");
        }
        txtTel.setEnabled(status);
        txtPoint.setEnabled(status);
    }

    private void enableFormProOnly(boolean status) {
        if (status == false) {
            txtPromotion.setText("0");
        }
        txtPromotion.setEnabled(status);
    }

    private void CheckMember() {
        if (rdioMem.isSelected()) {
            int totalPoint = (int) ((mem.getPoint() + earnPoint) - usedPoint);
            mem.setPoint(totalPoint);
            ms.updatePoint(mem);
            lbldisRemainingPoint.setText(totalPoint + " Point");
        }
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MembercrudDialog membercrudDialog = new MembercrudDialog(frame, editedMember);
        membercrudDialog.setLocationRelativeTo(this);
        membercrudDialog.setVisible(true);
    }
    private void openInvoiceDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        invoiceDialog invoicedialog = new invoiceDialog(frame, bill, discount);
        invoicedialog.setLocationRelativeTo(this);
        invoicedialog.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Header;
    private javax.swing.ButtonGroup Size_btnGroup;
    private javax.swing.ButtonGroup SweetLevel_btnGroup;
    private javax.swing.ButtonGroup Type_btnGroup;
    private javax.swing.JButton bekerybtn;
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton clearbtn;
    private javax.swing.JButton confirmbtn;
    private javax.swing.JButton drinkbtn;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblChange1;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalUnit;
    private javax.swing.JLabel lblTotalUnit1;
    private javax.swing.JLabel lblTotalUnit10;
    private javax.swing.JLabel lblTotalUnit2;
    private javax.swing.JLabel lblTotalUnit3;
    private javax.swing.JLabel lblTotalUnit4;
    private javax.swing.JLabel lblTotalUnit5;
    private javax.swing.JLabel lblTotalUnit6;
    private javax.swing.JLabel lblTotalUnit7;
    private javax.swing.JLabel lblTotalUnit8;
    private javax.swing.JLabel lblTotalUnit9;
    private javax.swing.JLabel lbldisChange;
    private javax.swing.JLabel lbldisD;
    private javax.swing.JLabel lbldisEarnPoint;
    private javax.swing.JLabel lbldisEarnPoint1;
    private javax.swing.JLabel lbldisMemName;
    private javax.swing.JLabel lbldisNP;
    private javax.swing.JLabel lbldisPS;
    private javax.swing.JLabel lbldisPromotionname;
    private javax.swing.JLabel lbldisRemainingPoint;
    private javax.swing.JLabel lbldisT;
    private javax.swing.JLabel lbldisTU;
    private javax.swing.JLabel lbldisTU1;
    private javax.swing.JLabel lbldismemPoint;
    private javax.swing.JLabel netbtn;
    private javax.swing.JButton promotionbtn;
    private javax.swing.JRadioButton rdioMem;
    private javax.swing.JRadioButton rdioPromotion;
    private javax.swing.JButton regismembtn;
    private javax.swing.JScrollPane scrMenuList;
    private javax.swing.JButton serachmembtn1;
    private javax.swing.JTable tblBillDetail;
    private javax.swing.JTextField txtCash;
    private javax.swing.JTextField txtCusName;
    private javax.swing.JTextField txtEmpName;
    private javax.swing.JTextField txtPoint;
    private javax.swing.JTextField txtPromotion;
    private javax.swing.JTextField txtTel;
    private javax.swing.JButton usepromotionbtn;
    // End of variables declaration//GEN-END:variables
    @Override
    public void buy(Menu menu, int qty) {
        bill.addBillDetail(menu, qty);
        refreshBill();
    }
}
