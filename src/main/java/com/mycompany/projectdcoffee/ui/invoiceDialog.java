/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;

import com.mycompany.projectdcoffee.model.Bill;
import com.mycompany.projectdcoffee.model.BillDetail;
import com.mycompany.projectdcoffee.model.CommonDateTime;
import com.mycompany.projectdcoffee.model.Employee;
import com.mycompany.projectdcoffee.model.Member;
import com.mycompany.projectdcoffee.model.Payment;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.service.BillDetailServices;
import com.mycompany.projectdcoffee.service.BillService;
import com.mycompany.projectdcoffee.service.EmployeeService;
import com.mycompany.projectdcoffee.service.MemberService;
import com.mycompany.projectdcoffee.service.PaymentService;
import com.mycompany.projectdcoffee.service.PromotionService;
import java.awt.Image;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ADMIN
 */
public class invoiceDialog extends javax.swing.JDialog {
    private final BillService billService = new BillService();
    private final MemberService ms = new MemberService();
    private final PromotionService ps = new PromotionService();
    private final EmployeeService es = new EmployeeService();
    private final BillDetailServices billDetailService = new BillDetailServices();
    private final PaymentService pays = new PaymentService();
    private MainMenuEmployee mmemp;
    private String empName;
    private Employee emp;
    private int earnPoint;
    private float usedPoint;
    private float discount = 0.0f;
    private Promotion pro;
    private Bill bill;
    private Member mem;
    private Payment payment;
    private String taxId;
    String[] dateTime = CommonDateTime.getDateTimeNowString().split(" ");
    /**
     * Creates new form invoiceDialog
     */
    public invoiceDialog(java.awt.Frame parent,Bill bill,float discount) {
        super(parent, true);
        initComponents();
        this.bill = bill;
        this.discount = discount;
        this.bill.setBillDetails((ArrayList<BillDetail>) billDetailService.getBillDetailsByBillId(bill.getId()));
        loadImage();
        RandomTaxID();
        initOrderTable();
        setInvoice();
        
    }
     private void loadImage() {
        ImageIcon icon = new ImageIcon("./Login" + ".png");
        Image image = icon.getImage();
        Image newImage = image.getScaledInstance(100, 100, image.SCALE_SMOOTH);
        icon.setImage(newImage);
        lblPhoto.setIcon(icon);
    }
    
    private void initOrderTable() {
        tblBillDetail.setModel(new AbstractTableModel() {
            String[] header = {"Name", "Price", "Qty", "Total"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return bill.getBillDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<BillDetail> billDetails = bill.getBillDetails();
                BillDetail billDetail = billDetails.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return billDetail.getMenuName();
                    case 1:
                        return billDetail.getMenuPrice();
                    case 2:
                        return billDetail.getQty();
                    case 3:
                        return billDetail.getTotalPrice();
                    default:
                        return "";
                }
            }
        });
    
    }
    
    public void setInvoice(){
        emp = es.getByID(bill.getEmpId());
        lblEmp.setText("Employee :"+emp.getEname());
        lblQueue.setText("Q"+bill.getId());
        lblTax.setText("TAX ID : "+taxId);
        lblDate.setText(dateTime[0]);
        lblTime.setText(dateTime[1]);
        lblTotalQty.setText("Quantity : "+bill.getTotalQty());
        lblTotal.setText(Double.toString(bill.getTotal()));
        lblDiscount.setText(discount+" Baht");
        lblNetPrice.setText(bill.getTotal()-discount+" Baht");
        lblCash.setText(bill.getCash()+" Baht");
        lblChange.setText(bill.getCash()-(bill.getTotal()-discount)+ " Baht");
        if(bill.getMemberId() != 0){
            mem = ms.getById(bill.getMemberId());
            lbldisMemName.setText(mem.getName());
            lbldisMemTel.setText(mem.getTel());
            lbldisRemainPoint.setText(Integer.toString(mem.getPoint()));
        }
        payment = pays.getById(bill.getPmId());
        lblPm.setText("Payment Method :"+payment.getType());
    }
    
    public void RandomTaxID() {
        Random random = new Random();
        StringBuilder tax_Id = new StringBuilder();
        for (int i = 0; i < 13; i++) {
            int digit = random.nextInt(10);
            tax_Id.append(digit);
        }
        taxId = tax_Id.toString();

    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBillDetail = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        lblQueue = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblLocal = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblSc = new javax.swing.JLabel();
        lblDate2 = new javax.swing.JLabel();
        lblEmp = new javax.swing.JLabel();
        lblTax = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblTotalQty = new javax.swing.JLabel();
        lblMemName = new javax.swing.JLabel();
        lblMemRemainPoint = new javax.swing.JLabel();
        lblMemtal = new javax.swing.JLabel();
        lblTotalQty1 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblTotalQty3 = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotalQty5 = new javax.swing.JLabel();
        lblNetPrice = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblPm = new javax.swing.JLabel();
        lblTotalQty6 = new javax.swing.JLabel();
        lblCash = new javax.swing.JLabel();
        lblTotalQty7 = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        lblLocal1 = new javax.swing.JLabel();
        lblPhoto = new javax.swing.JLabel();
        lblDate11 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblTime = new javax.swing.JLabel();
        lbldisRemainPoint = new javax.swing.JLabel();
        lbldisMemName = new javax.swing.JLabel();
        lbldisMemTel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setForeground(new java.awt.Color(0, 0, 0));

        tblBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblBillDetail);

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Queue");

        lblQueue.setFont(new java.awt.Font("sansserif", 0, 14)); // NOI18N
        lblQueue.setForeground(new java.awt.Color(0, 0, 0));
        lblQueue.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblQueue.setText("-");

        jLabel2.setFont(new java.awt.Font("sansserif", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("D-Coffee");

        lblLocal.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        lblLocal.setForeground(new java.awt.Color(0, 0, 0));
        lblLocal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLocal.setText("Burapha University, Bang Saen");

        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("--------------------------------------------------------------------------------------------------------------------------");

        jLabel3.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Invoice");

        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("--------------------------------------------------------------------------------------------------------------------------");

        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("--------------------------------------------------------------------------------------------------------------------------");

        lblSc.setForeground(new java.awt.Color(0, 0, 0));
        lblSc.setText("Store");

        lblDate2.setForeground(new java.awt.Color(0, 0, 0));
        lblDate2.setText("Date :");

        lblEmp.setForeground(new java.awt.Color(0, 0, 0));
        lblEmp.setText("Employee :");

        lblTax.setForeground(new java.awt.Color(0, 0, 0));
        lblTax.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTax.setText("TAX ID :");

        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("--------------------------------------------------------------------------------------------------------------------------");

        jLabel8.setForeground(new java.awt.Color(0, 0, 0));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("--------------------------------------------------------------------------------------------------------------------------");

        lblTotalQty.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty.setText("Quantity :");

        lblMemName.setForeground(new java.awt.Color(0, 0, 0));
        lblMemName.setText("Member Name :");

        lblMemRemainPoint.setForeground(new java.awt.Color(0, 0, 0));
        lblMemRemainPoint.setText("Remaining Point :");

        lblMemtal.setForeground(new java.awt.Color(0, 0, 0));
        lblMemtal.setText("Member Tel :");

        lblTotalQty1.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty1.setText("Total :");

        lblTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("- Baht");

        lblTotalQty3.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty3.setText("Discount :");

        lblDiscount.setForeground(new java.awt.Color(0, 0, 0));
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("- Baht");

        lblTotalQty5.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty5.setText("Net Price :");

        lblNetPrice.setForeground(new java.awt.Color(0, 0, 0));
        lblNetPrice.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNetPrice.setText("- Baht");

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("==========================");

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("--------------------------------------------------------------------------------------------------------------------------");

        lblPm.setForeground(new java.awt.Color(0, 0, 0));
        lblPm.setText("Payment Method :");

        lblTotalQty6.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty6.setText("Cash :");

        lblCash.setForeground(new java.awt.Color(0, 0, 0));
        lblCash.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblCash.setText("- Baht");

        lblTotalQty7.setForeground(new java.awt.Color(0, 0, 0));
        lblTotalQty7.setText("Change :");

        lblChange.setForeground(new java.awt.Color(0, 0, 0));
        lblChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblChange.setText("- Baht");

        jButton1.setForeground(new java.awt.Color(0, 0, 0));
        jButton1.setText("Close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lblLocal1.setFont(new java.awt.Font("sansserif", 0, 18)); // NOI18N
        lblLocal1.setForeground(new java.awt.Color(0, 0, 0));
        lblLocal1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLocal1.setText(" 169 Long Hat Bang Saen Road, Saen Sukt, Chonburi 20131");

        lblPhoto.setBackground(new java.awt.Color(204, 255, 204));
        lblPhoto.setOpaque(true);

        lblDate11.setForeground(new java.awt.Color(0, 0, 0));
        lblDate11.setText("Time :");

        lblDate.setText("-");

        lblTime.setText("-");

        lbldisRemainPoint.setForeground(new java.awt.Color(0, 0, 0));
        lbldisRemainPoint.setText("-");

        lbldisMemName.setForeground(new java.awt.Color(0, 0, 0));
        lbldisMemName.setText("-");

        lbldisMemTel.setForeground(new java.awt.Color(0, 0, 0));
        lbldisMemTel.setText("-");

        jLabel11.setFont(new java.awt.Font("sansserif", 1, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(0, 0, 0));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Have a nice day!");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLocal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblQueue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTotalQty, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMemName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblMemRemainPoint, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(lblMemtal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbldisRemainPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbldisMemTel, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblTotalQty1)
                                        .addGap(18, 18, 18)
                                        .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblTotalQty5)
                                        .addGap(18, 18, 18)
                                        .addComponent(lblNetPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lbldisMemName, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblSc, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTax, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblLocal1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(lblPm, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblTotalQty7)
                                    .addComponent(lblTotalQty6))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblCash, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblChange, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(lblTotalQty3)
                                .addGap(18, 18, 18)
                                .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblEmp, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblDate2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblDate11)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(261, 261, 261)
                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblQueue)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblLocal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblLocal1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSc)
                    .addComponent(lblTax))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblEmp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDate2)
                    .addComponent(lblDate11)
                    .addComponent(lblDate)
                    .addComponent(lblTime))
                .addGap(3, 3, 3)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalQty)
                    .addComponent(lblTotalQty1)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalQty3)
                    .addComponent(lblDiscount))
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMemName)
                    .addComponent(lbldisMemName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMemtal)
                    .addComponent(lblTotalQty5)
                    .addComponent(lblNetPrice)
                    .addComponent(lbldisMemTel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMemRemainPoint)
                        .addComponent(lbldisRemainPoint)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalQty6)
                    .addComponent(lblCash))
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPm)
                    .addComponent(lblTotalQty7)
                    .addComponent(lblChange))
                .addGap(27, 27, 27)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDate11;
    private javax.swing.JLabel lblDate2;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblEmp;
    private javax.swing.JLabel lblLocal;
    private javax.swing.JLabel lblLocal1;
    private javax.swing.JLabel lblMemName;
    private javax.swing.JLabel lblMemRemainPoint;
    private javax.swing.JLabel lblMemtal;
    private javax.swing.JLabel lblNetPrice;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JLabel lblPm;
    private javax.swing.JLabel lblQueue;
    private javax.swing.JLabel lblSc;
    private javax.swing.JLabel lblTax;
    private javax.swing.JLabel lblTime;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblTotalQty;
    private javax.swing.JLabel lblTotalQty1;
    private javax.swing.JLabel lblTotalQty3;
    private javax.swing.JLabel lblTotalQty5;
    private javax.swing.JLabel lblTotalQty6;
    private javax.swing.JLabel lblTotalQty7;
    private javax.swing.JLabel lbldisMemName;
    private javax.swing.JLabel lbldisMemTel;
    private javax.swing.JLabel lbldisRemainPoint;
    private javax.swing.JTable tblBillDetail;
    // End of variables declaration//GEN-END:variables
}
