/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.projectdcoffee.ui;



import com.mycompany.projectdcoffee.model.Rawmaterial;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import com.mycompany.projectdcoffee.service.RawMaterialService;
import com.mycompany.projectdcoffee.service.RecieptService;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Admin
 */
public class RecieptDetailOrderDialog extends javax.swing.JDialog {

    private RecieptDetail edtRecieptDetail;
    private List<RecieptDetail> list;
    private String id;
    private RawMaterialService edtRawMaterial;
    private Rawmaterial rawMaterial;
    private RawMaterialService RawMaterialService;

    /**
     * Creates new form RecieptDetailOrderDialog
     */
    public RecieptDetailOrderDialog(java.awt.Frame parent, RecieptDetail edtRecieptDetail) {
        super(parent, true);
        initComponents();
        this.edtRecieptDetail = edtRecieptDetail;
        generateTable();
        enableForm(false);
        enableBtn(false);
    }

    private void generateTable() {
        RecieptService rs = new RecieptService();
        list = rs.getRecieptDetail();

        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] header = {"ID", "RecieptID", "RawMaterialID", "QTY", "Unit/Price", "Discount", "Total", "Net"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 8;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
//                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
//                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                RecieptDetail recieptDetailtbl = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetailtbl.getId();
                    case 1:
                        return recieptDetailtbl.getRecieptId();
                    case 2:
                        return recieptDetailtbl.getRmId();
                    case 3:
                        return recieptDetailtbl.getQuantity();
                    case 4:
                        return recieptDetailtbl.getUnitPrice();
                    case 5:
                        return recieptDetailtbl.getDiscount();
                    case 6:
                        return recieptDetailtbl.getTotal();
                    case 7:
                        return recieptDetailtbl.getNet();
                    default:
                        return "";
                }
            }
        });
    }
    
    private void generateTableById() {
        RecieptService rdd = new RecieptService();
        id = edtListRecieptID.getText();
        list = rdd.getAllRecieptDetails(id);

        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] header = {"ID", "RecieptID", "RawMaterialID", "QTY", "Unit/Price", "Discount", "Total", "Net"};

            @Override
            public String getColumnName(int column) {
                return header[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 8;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
//                ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
//                RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                RecieptDetail recieptDetailtbl = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return recieptDetailtbl.getId();
                    case 1:
                        return recieptDetailtbl.getRecieptId();
                    case 2:
                        return recieptDetailtbl.getRmId();
                    case 3:
                        return recieptDetailtbl.getQuantity();
                    case 4:
                        return recieptDetailtbl.getUnitPrice();
                    case 5:
                        return recieptDetailtbl.getDiscount();
                    case 6:
                        return recieptDetailtbl.getTotal();
                    case 7:
                        return recieptDetailtbl.getNet();
                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRecieptId = new javax.swing.JLabel();
        lblQty = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();
        edtRecieptId = new javax.swing.JTextField();
        edtQty = new javax.swing.JTextField();
        edtDiscount = new javax.swing.JTextField();
        lblRmId = new javax.swing.JLabel();
        edtRmId = new javax.swing.JTextField();
        lblListRecipetID = new javax.swing.JLabel();
        edtListRecieptID = new javax.swing.JTextField();
        btnGenerate = new javax.swing.JButton();
        btnGetAllRecieptDetail = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        lblUnitPrice = new javax.swing.JLabel();
        edtUnitPrice = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        lblUnitPrice1 = new javax.swing.JLabel();
        lblUnitPrice2 = new javax.swing.JLabel();
        btnCaculate = new javax.swing.JButton();
        lblValueTotal = new javax.swing.JLabel();
        lblValueNet = new javax.swing.JLabel();
        lblRdId = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();

        jButton4.setText("ADD");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(231, 198, 152));
        jPanel1.setForeground(new java.awt.Color(0, 0, 0));

        lblRecieptId.setForeground(new java.awt.Color(0, 0, 0));
        lblRecieptId.setText("RecieptId:");

        lblQty.setForeground(new java.awt.Color(0, 0, 0));
        lblQty.setText("QTY:");

        lblDiscount.setForeground(new java.awt.Color(0, 0, 0));
        lblDiscount.setText("Discount:");

        btnSave.setBackground(new java.awt.Color(51, 255, 51));
        btnSave.setForeground(new java.awt.Color(0, 0, 0));
        btnSave.setText("SAVE");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblRecieptDetail);

        lblRmId.setForeground(new java.awt.Color(0, 0, 0));
        lblRmId.setText("RmId:");

        lblListRecipetID.setForeground(new java.awt.Color(0, 0, 0));
        lblListRecipetID.setText("List By RecieptID:");

        btnGenerate.setText("GENERATE");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnGetAllRecieptDetail.setText("ALL");
        btnGetAllRecieptDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGetAllRecieptDetailActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 51, 51));
        btnClear.setForeground(new java.awt.Color(0, 0, 0));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        lblUnitPrice.setForeground(new java.awt.Color(0, 0, 0));
        lblUnitPrice.setText("UnitPrice:");

        btnAdd.setText("ADD");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnEdit.setText("EDIT");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setText("DELETE");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        lblUnitPrice1.setForeground(new java.awt.Color(0, 0, 0));
        lblUnitPrice1.setText("TOTAL:");

        lblUnitPrice2.setForeground(new java.awt.Color(0, 0, 0));
        lblUnitPrice2.setText("NET:");

        btnCaculate.setText("CALCULATE");
        btnCaculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCaculateActionPerformed(evt);
            }
        });

        lblValueTotal.setBackground(new java.awt.Color(0, 0, 0));
        lblValueTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblValueTotal.setText("0");

        lblValueNet.setBackground(new java.awt.Color(0, 0, 0));
        lblValueNet.setForeground(new java.awt.Color(0, 0, 0));
        lblValueNet.setText("0");

        lblRdId.setForeground(new java.awt.Color(0, 0, 0));
        lblRdId.setText("ID:");

        btnClose.setText("Close");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 759, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblRecieptId)
                                    .addComponent(lblQty))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(edtQty)
                                    .addComponent(edtRecieptId)))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(lblDiscount)
                                .addGap(9, 9, 9)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(btnSave)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(btnCaculate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(26, 26, 26)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblRmId)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(edtRmId, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblUnitPrice)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(edtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(41, 41, 41)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(lblUnitPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(lblValueTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(lblUnitPrice2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(lblValueNet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblListRecipetID, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblRdId)
                                        .addGap(93, 93, 93)))))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btnGetAllRecieptDetail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(edtListRecieptID, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(38, 38, 38))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblRecieptId)
                        .addComponent(edtRecieptId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblRmId)
                        .addComponent(edtRmId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(edtListRecieptID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblListRecipetID)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnGenerate)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblQty)
                                .addComponent(edtQty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblUnitPrice)
                                .addComponent(edtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblDiscount)
                                    .addComponent(edtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnGetAllRecieptDetail))
                                .addGap(13, 13, 13)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnSave)
                                    .addComponent(btnClear)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblUnitPrice1)
                                    .addComponent(lblValueTotal))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblUnitPrice2)
                                    .addComponent(lblValueNet))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCaculate))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(32, 32, 32)
                        .addComponent(lblRdId)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnEdit)
                    .addComponent(btnDelete)
                    .addComponent(btnClose)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        RawMaterialService = new RawMaterialService();
        RecieptService rs = new RecieptService();
        int oldQuantity = edtRecieptDetail.getQuantity();
        if (edtRecieptDetail.getId() < 0) {// Add New
            setFormToObject();
            rs.addNewDetail(edtRecieptDetail);
            updateRawmaterial();
        } else {
            setFormToObject();
            rs.updateDetail(edtRecieptDetail);
            updateEdtRawmaterial(oldQuantity);
        }
        enableForm(false);
        enableBtn(false);
        refreshTable();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        generateTableById();
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void btnGetAllRecieptDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGetAllRecieptDetailActionPerformed
        generateTable();
        edtListRecieptID.setText("");
        enableForm(false);
    }//GEN-LAST:event_btnGetAllRecieptDetailActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        enableForm(false);
        edtRecieptDetail = null;
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblRecieptDetail.getSelectedRow();
        if (selectedIndex >= 0) {
            edtRecieptDetail = list.get(selectedIndex);
            setObjectToForm();
            enableForm(true);
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        edtRecieptDetail = new RecieptDetail();
        enableForm(true);
            if (edtListRecieptID.getText() != null) {
            String recieptId = edtListRecieptID.getText();
            edtRecieptId.setText(recieptId);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblRecieptDetail.getSelectedRow();
        if (selectedIndex >= 0) {
            edtRecieptDetail = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "DO you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                RecieptService rs = new RecieptService();
                rs.deleteDetail(edtRecieptDetail);
            }
        }
        refreshTable();     
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnCaculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCaculateActionPerformed
        float dis = Float.parseFloat(edtDiscount.getText());
        float unitP = Float.parseFloat(edtUnitPrice.getText());
        int qty = Integer.parseInt(edtQty.getText());
        
        
        float total = (unitP*qty)-dis;
        float net = total-(total * 0.07f);
        
        lblValueTotal.setText(Float.toString(total));
        lblValueNet.setText(Float.toString(net));
        enableBtn(true);
    }//GEN-LAST:event_btnCaculateActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCaculate;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnGetAllRecieptDetail;
    private javax.swing.JButton btnSave;
    private javax.swing.JTextField edtDiscount;
    private javax.swing.JTextField edtListRecieptID;
    private javax.swing.JTextField edtQty;
    private javax.swing.JTextField edtRecieptId;
    private javax.swing.JTextField edtRmId;
    private javax.swing.JTextField edtUnitPrice;
    private javax.swing.JButton jButton4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblListRecipetID;
    private javax.swing.JLabel lblQty;
    private javax.swing.JLabel lblRdId;
    private javax.swing.JLabel lblRecieptId;
    private javax.swing.JLabel lblRmId;
    private javax.swing.JLabel lblUnitPrice;
    private javax.swing.JLabel lblUnitPrice1;
    private javax.swing.JLabel lblUnitPrice2;
    private javax.swing.JLabel lblValueNet;
    private javax.swing.JLabel lblValueTotal;
    private javax.swing.JTable tblRecieptDetail;
    // End of variables declaration//GEN-END:variables

    private void setObjectToForm() {
        edtRecieptId.setText(Integer.toString(edtRecieptDetail.getRecieptId()));
        edtRmId.setText(Integer.toString(edtRecieptDetail.getRmId()));
        edtDiscount.setText(Float.toString(edtRecieptDetail.getDiscount()));
        edtQty.setText(Integer.toString(edtRecieptDetail.getQuantity()));
        edtUnitPrice.setText(Float.toString(edtRecieptDetail.getUnitPrice()));
    }

    private void setFormToObject() {
        edtRecieptDetail.setRecieptId(Integer.parseInt(edtRecieptId.getText()));
        edtRecieptDetail.setRmId(Integer.parseInt(edtRmId.getText()));
        edtRecieptDetail.setDiscount(Float.parseFloat(edtDiscount.getText()));
        edtRecieptDetail.setQuantity(Integer.parseInt(edtQty.getText()));
        edtRecieptDetail.setUnitprice(Float.parseFloat(edtUnitPrice.getText()));
        edtRecieptDetail.setTotal(Float.parseFloat(lblValueTotal.getText()));
        edtRecieptDetail.setNet(Float.parseFloat(lblValueTotal.getText()));
    }
    
    private void refreshTable() {
        RecieptService rs = new RecieptService();
        list = rs.getRecieptDetail();
        tblRecieptDetail.invalidate();
        tblRecieptDetail.repaint();
    }

    private void enableForm(boolean status) {
        if (status == false) {
            edtRecieptId.setText("");
            edtRmId.setText("");
            edtDiscount.setText("");
            edtQty.setText("");
            edtUnitPrice.setText("");
            lblValueTotal.setText("0");
            lblValueNet.setText("0");
        }
        edtRecieptId.setEnabled(status);
        edtRmId.setEnabled(status);
        edtDiscount.setEnabled(status);
        btnClear.setEnabled(status);
        edtQty.setEnabled(status);
        edtUnitPrice.setEnabled(status);
    }
    
    private void enableBtn(boolean status){
        btnSave.setEnabled(status);
    }
    
    public Rawmaterial updateRawmaterial(){
        RawMaterialService rms = new RawMaterialService();
        int rmid = Integer.parseInt(edtRmId.getText());
        rawMaterial = rms.getById(rmid);
        int qtyRawmaterial = Integer.parseInt(edtQty.getText());
        rawMaterial.setRawQoh(rawMaterial.getRawQoh()+qtyRawmaterial);
        float uPrice = Float.parseFloat(edtUnitPrice.getText());
        rawMaterial.setRawPrice(uPrice);
        return rms.updateQoh(rawMaterial);
    }
    
    public Rawmaterial updateEdtRawmaterial(int oldquantity){
        RawMaterialService rms = new RawMaterialService();
        int rmid = Integer.parseInt(edtRmId.getText());
        rawMaterial = rms.getById(rmid);
        int qtyRawmaterial = Integer.parseInt(edtQty.getText());
        float uPrice = Float.parseFloat(edtUnitPrice.getText());
        if(qtyRawmaterial > rawMaterial.getRawQoh()){
            rawMaterial.setRawQoh(rawMaterial.getRawQoh()+(qtyRawmaterial-oldquantity));
            rawMaterial.setRawPrice(uPrice);
        }else{
            rawMaterial.setRawQoh(rawMaterial.getRawQoh() - (oldquantity-qtyRawmaterial));
            rawMaterial.setRawPrice(uPrice);
        }
        return rms.updateQoh(rawMaterial);   
    }
}
