/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.PaymentDao;
import com.mycompany.projectdcoffee.model.Payment;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class PaymentService {
    public Payment getById(int id) {
        PaymentDao paymentDao = new PaymentDao();
        return paymentDao.get(id);
    }
    
    public List<Payment> getPayments(){
        PaymentDao paymentDao = new PaymentDao();
        return paymentDao.getAll(" pm_id asc");
    }

    public Payment addNew(Payment editedPayment) {
        PaymentDao paymentDao = new PaymentDao();
        return paymentDao.save(editedPayment);
    }

    public Payment update(Payment editedPayment) {
        PaymentDao paymentDao = new PaymentDao();
        return paymentDao.update(editedPayment);
    }

    public int delete(Payment editedPayment) {
        PaymentDao paymentDao = new PaymentDao();
        return paymentDao.delete(editedPayment);
    }
}
