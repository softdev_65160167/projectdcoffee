/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.SalaryDao;
import com.mycompany.projectdcoffee.model.Salary;
import java.util.List;

/**
 *
 * @author This PC
 */
public class SalaryService {
   
    public List<Salary> getSalarys(){
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.getAll(" salary_slipid asc");
    }

    public Salary addNew(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.save(editedSalary);
    }

    public Salary update(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.update(editedSalary);
    }

    public int delete(Salary editedSalary) {
        SalaryDao salaryDao = new SalaryDao();
        return salaryDao.delete(editedSalary);
    }
}
