/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.SalaryReportDao;
import com.mycompany.projectdcoffee.model.SalaryReport;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryReportService {
    
public List<SalaryReport> getSalaryReports(){
        SalaryReportDao salaryReportDao = new SalaryReportDao();
        return salaryReportDao.getAll(" salaryReport_id");
    }

    public SalaryReport addNew(SalaryReport editedSalaryReport) {
        SalaryReportDao salaryReportDao = new SalaryReportDao();
        return salaryReportDao.save(editedSalaryReport);
    }

    public SalaryReport update(SalaryReport editedSalaryReport) {
        SalaryReportDao salaryReportDao = new SalaryReportDao();
        return salaryReportDao.update(editedSalaryReport);
    }

    public int delete(SalaryReport editedSalaryReport) {
        SalaryReportDao salaryReportDao = new SalaryReportDao();
        return salaryReportDao.delete(editedSalaryReport);
    }
    
    
    
}
