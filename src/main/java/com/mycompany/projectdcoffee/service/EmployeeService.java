/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.EmployeeDao;
import com.mycompany.projectdcoffee.dao.EmployeeDao;
import com.mycompany.projectdcoffee.model.Employee;
import com.mycompany.projectdcoffee.model.Employee;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author This PC
 */
public class EmployeeService {
    
    public ArrayList<Employee> getAll() {
        EmployeeDao employeeDao = new EmployeeDao();
        return (ArrayList<Employee>) employeeDao.getAll();
    }
    public Employee getByName(String name) {
        EmployeeDao EmployeeDao = new EmployeeDao();
        Employee employee = EmployeeDao.getByName(name);
        return employee;
    }
    
    public Employee getByID(int id) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.get(id);
    }
    
    public List<Employee> getEmployees(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_id asc");
    }

    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

}
