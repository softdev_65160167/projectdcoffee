/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.MenuDao;
import com.mycompany.projectdcoffee.model.Menu;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class MenuService {
    
    private MenuDao productDao = new MenuDao();
    public ArrayList<Menu> getMenusOrderByName(){
        return (ArrayList<Menu>) productDao.getAll(" menu_name ASC ");
    }
    public ArrayList<Menu> getMenusCategoryDrink(){
        return (ArrayList<Menu>) productDao.getAllCategory(1);
    }
    
     public ArrayList<Menu> getMenusCategoryBakery(){
        return (ArrayList<Menu>) productDao.getAllCategory(2);
    }

    
    public Menu getById(int id) {
        MenuDao menuDao = new MenuDao();
        return menuDao.get(id);
    }
    
    public List<Menu> getMenus(){
        MenuDao menuDao = new MenuDao();
        return menuDao.getAll(" menu_id asc");
    }

    public Menu addNew(Menu editedMenu) {
        MenuDao menuDao = new MenuDao();
        return menuDao.save(editedMenu);
    }

    public Menu update(Menu editedMenu) {
        MenuDao menuDao = new MenuDao();
        return menuDao.update(editedMenu);
    }

    public int delete(Menu editedMenu) {
        MenuDao menuDao = new MenuDao();
        return menuDao.delete(editedMenu);
    }
}
