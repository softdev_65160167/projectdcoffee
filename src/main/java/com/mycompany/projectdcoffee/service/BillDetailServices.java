
package com.mycompany.projectdcoffee.service;


import com.mycompany.projectdcoffee.dao.BillDetailDao;
import com.mycompany.projectdcoffee.model.BillDetail;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BillDetailServices {

    public ArrayList<BillDetail> getAll() {
        BillDetailDao billDetailDao = new BillDetailDao();
        return (ArrayList<BillDetail>) billDetailDao.getAll();
    }

    public List<BillDetail> getUsersByOrder(String where, String order) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.getAll(where, order);
    }

    public List<BillDetail> getBillDetailsByBillId(int id) {
        BillDetailDao billDetailDao = new BillDetailDao();
        return billDetailDao.getAllByBillId(id);
    }

}
