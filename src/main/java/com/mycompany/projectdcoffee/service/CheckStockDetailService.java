/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.CheckStockDetailDao;
import com.mycompany.projectdcoffee.model.CheckStockDetail;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class CheckStockDetailService {
    public List<CheckStockDetail> getCheckStockDetails(){
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.getAll(" crd_id asc");
    }

    public CheckStockDetail addNew(CheckStockDetail editedCheckStockDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.save(editedCheckStockDetail);
    }

    public CheckStockDetail update(CheckStockDetail editedCheckStockDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.update(editedCheckStockDetail);
    }

    public int delete(CheckStockDetail editedCheckStockDetail) {
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        return checkStockDetailDao.delete(editedCheckStockDetail);
    }
}
