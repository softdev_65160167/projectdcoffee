/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.PromotionDao;
import com.mycompany.projectdcoffee.dao.PromotionDetailDao;
import com.mycompany.projectdcoffee.model.Promotion;
import com.mycompany.projectdcoffee.model.PromotionDetail;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class PromotionService {
    public Promotion getByid(int id){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.get(id);    
    }
    public PromotionDetail getByPromotionDetailsId(int id){
        PromotionDetailDao rdd =new PromotionDetailDao();
        return rdd.get(id);
    }
    public List<PromotionDetail> getPromotionDetail(){
        PromotionDetailDao rdd =new PromotionDetailDao();
        return rdd.getAll(" promotionDetail_id asc");
    }
    public List<PromotionDetail> getAllPromotionDetail(String id){
        PromotionDetailDao rdd =new PromotionDetailDao();
        return rdd.getAll("promotion_id ="+id,"promotionDetail_id asc");
    }
    public PromotionDetail addNewDetail(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao rdd = new PromotionDetailDao();
        return rdd.save(editedPromotionDetail);
    }
     public PromotionDetail updateDetail(PromotionDetail editedPromotionDetail) {
        PromotionDetailDao rdd = new PromotionDetailDao();
        return rdd.update(editedPromotionDetail);
    }
    public List<Promotion> getPromotions(){
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(" promotion_id asc ");
    }
    public Promotion addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.delete(editedPromotion);
    }
}
