/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.RecieptDao;
import com.mycompany.projectdcoffee.dao.RecieptDetailDao;
import com.mycompany.projectdcoffee.model.Reciept;
import com.mycompany.projectdcoffee.model.RecieptDetail;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RecieptService {
    public Reciept getById(int id) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.get(id);
    }
    
    public List<Reciept> getAllRecieptsById(String id){
        RecieptDao rd = new RecieptDao();
        return rd.getAll("reciept_id = " + id, " reciept_id asc");
    }
    
    public RecieptDetail getByRecieptDetailsId(int id) {
        RecieptDetailDao rdd = new RecieptDetailDao();
        return rdd.get(id);
    }
    
    public List<RecieptDetail> getRecieptDetail(){
        RecieptDetailDao rdd = new RecieptDetailDao();
        return rdd.getAll( " recieptdetail_id asc");
    }
    
    public List<RecieptDetail> getAllRecieptDetails(String id){
       RecieptDetailDao rdd = new RecieptDetailDao();
       return rdd.getAll("reciept_id = "+id, " recieptdetail_id asc");
    }
    
    public List<Reciept> getReciepts(){
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.getAll(" reciept_id asc");
    }
    
    public RecieptDetail addNewDetail(RecieptDetail edtRecieptDetail){
        RecieptDetailDao rdd = new RecieptDetailDao();
        return rdd.save(edtRecieptDetail);
    }
    
    public RecieptDetail updateDetail(RecieptDetail edtRecieptDetail){
        RecieptDetailDao rdd = new RecieptDetailDao();
        return rdd.update(edtRecieptDetail);
    }
    
    public int deleteDetail(RecieptDetail edtRecieptDetail){
        RecieptDetailDao rdd = new RecieptDetailDao();
        return rdd.delete(edtRecieptDetail);
    }

    public Reciept addNew(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.save(editedReciept);
    }

    public Reciept update(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.update(editedReciept);
    }

    public int delete(Reciept editedReciept) {
        RecieptDao recieptDao = new RecieptDao();
        return recieptDao.delete(editedReciept);
    }
}
