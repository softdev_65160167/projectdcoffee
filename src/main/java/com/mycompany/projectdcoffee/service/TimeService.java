/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

/**
 *
 * @author chano
 */

import com.mycompany.projectdcoffee.dao.TimeDAO;
import com.mycompany.projectdcoffee.model.Time;

import java.util.List;

public class TimeService {
    private final TimeDAO timeDAO;

    public TimeService(TimeDAO timeDAO) {
        this.timeDAO = timeDAO;
    }

    public void updateRealTime() {
        timeDAO.updateRealTime();
    }

    public void resetRealTime() {
        timeDAO.resetRealTime();
    }

    public List<Time> getAllTime() {
        return timeDAO.getAllTime();
    }
}


