/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.CheckStockListDao;
import com.mycompany.projectdcoffee.model.CheckStockList;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class CheckStockListService {
    
    public List<CheckStockList> getCheckStockLists(){
        CheckStockListDao checkStockListDao = new CheckStockListDao();
        return checkStockListDao.getAll(" csl_id asc");
    }

    public CheckStockList addNew(CheckStockList editedCheckStockList) {
        CheckStockListDao checkStockListDao = new CheckStockListDao();
        return checkStockListDao.save(editedCheckStockList);
    }

    public CheckStockList update(CheckStockList editedCheckStockList) {
        CheckStockListDao checkStockListDao = new CheckStockListDao();
        return checkStockListDao.update(editedCheckStockList);
    }

    public int delete(CheckStockList editedCheckStockList) {
        CheckStockListDao checkStockListDao = new CheckStockListDao();
        return checkStockListDao.delete(editedCheckStockList);
    }
}
