/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.MemberDao;
import com.mycompany.projectdcoffee.dao.MemberDao;
import com.mycompany.projectdcoffee.model.Member;
import com.mycompany.projectdcoffee.model.Member;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author This PC
 */
public class MemberService {

    public ArrayList<Member> getAll() {
        MemberDao bill = new MemberDao();
        return (ArrayList<Member>) bill.getAll();
    }
    
    public Member getByTel(String tel) {
        MemberDao MemberDao = new MemberDao();
        Member member = MemberDao.getByTel(tel);
        return member;
    }
    
    public Member getById(int id) {
        MemberDao memberDao = new MemberDao();
        return memberDao.get(id);
    }

    public List<Member> getMembers() {
        MemberDao MemberDao = new MemberDao();
        return MemberDao.getAll(" member_id asc");
    }

    public Member addNew(Member editedMember) {
        MemberDao MemberDao = new MemberDao();
        return MemberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao MemberDao = new MemberDao();
        return MemberDao.update(editedMember);
    }

    public Member updatePoint(Member editedMember) {
        MemberDao MemberDao = new MemberDao();
        return MemberDao.updatePoint(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao MemberDao = new MemberDao();
        return MemberDao.delete(editedMember);
    }

   
}
