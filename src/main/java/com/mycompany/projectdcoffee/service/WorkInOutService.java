/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.WorkInOutDAO;
import com.mycompany.projectdcoffee.model.WorkInOut;
import java.util.List;

public class WorkInOutService {

    private final WorkInOutDAO workInOutDAO;

    public WorkInOutService(WorkInOutDAO workInOutDAO) {
        this.workInOutDAO = workInOutDAO;
    }

    public WorkInOut getWorkInOut(int id) {
        return workInOutDAO.get(id);
    }

    public List<WorkInOut> getAllWorkInOut() {
        return workInOutDAO.getAll();
    }

    public List<WorkInOut> getAllWorkInOut(String where, String order) {
        return workInOutDAO.getAll(where, order);
    }

    public List<WorkInOut> getAllWorkInOut(String order) {
        return workInOutDAO.getAll(order);
    }

    public boolean addNewWorkInOut(WorkInOut workInOut) {
        return workInOutDAO.save(workInOut);
    }

    public boolean updateWorkInOut(WorkInOut workInOut) {
        return workInOutDAO.update(workInOut);
    }

    public boolean deleteWorkInOut(int id) {
        return workInOutDAO.delete(id);
    }

    
    public int getEmployeeIdByEmployeeName(String loggedInUsername) {
        return workInOutDAO.getEmployeeIdByEmployeeName(loggedInUsername);
    }
}
