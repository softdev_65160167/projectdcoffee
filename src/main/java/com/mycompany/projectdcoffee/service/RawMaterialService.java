/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.RawmaterialDao;
import com.mycompany.projectdcoffee.model.Rawmaterial;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class RawMaterialService {
    public ArrayList<Rawmaterial> getAll() {
        RawmaterialDao bill = new RawmaterialDao();
        return (ArrayList<Rawmaterial>) bill.getAll();
    }
    public Rawmaterial getById(int id) {
        RawmaterialDao rawMaterialDao = new RawmaterialDao();
        return rawMaterialDao.get(id);
    }
    
    public List<Rawmaterial> getReciepts(){
        RawmaterialDao rawMaterialDao = new RawmaterialDao();
        return rawMaterialDao.getAll(" rm_id asc");
    }

    public Rawmaterial addNew(Rawmaterial editedRawMaterial) {
        RawmaterialDao rawMaterialDao = new RawmaterialDao();
        return rawMaterialDao.save(editedRawMaterial);
    }

    public Rawmaterial update(Rawmaterial editedRawMaterial) {
        RawmaterialDao rawMaterialDao = new RawmaterialDao();
        return rawMaterialDao.update(editedRawMaterial);
    }
    
    public Rawmaterial updateQoh(Rawmaterial edtRawmaterial){
        RawmaterialDao rd = new RawmaterialDao();
        return rd.updateQoh(edtRawmaterial);
    }

    public int delete(Rawmaterial editedRawMaterial) {
        RawmaterialDao rawMaterialDao = new RawmaterialDao();
        return rawMaterialDao.delete(editedRawMaterial);
    }
}
