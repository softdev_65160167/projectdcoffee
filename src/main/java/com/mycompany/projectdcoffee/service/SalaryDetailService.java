/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.SalaryDetailDao;
import com.mycompany.projectdcoffee.model.SalaryDetail;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class SalaryDetailService {
    
public List<SalaryDetail> getSalaryDetails(){
        SalaryDetailDao salaryDao = new SalaryDetailDao();
        return salaryDao.getAll(" salaryDetail_id asc");
    }

    public SalaryDetail addNew(SalaryDetail editedSalaryDetail) {
        SalaryDetailDao salaryDao = new SalaryDetailDao();
        return salaryDao.save(editedSalaryDetail);
    }

    public SalaryDetail update(SalaryDetail editedSalaryDetail) {
        SalaryDetailDao salaryDao = new SalaryDetailDao();
        return salaryDao.update(editedSalaryDetail);
    }

    public int delete(SalaryDetail editedSalaryDetail) {
        SalaryDetailDao salaryDao = new SalaryDetailDao();
        return salaryDao.delete(editedSalaryDetail);
    }
    
    
    
}
