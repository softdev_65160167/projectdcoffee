/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.FeeDao;
import com.mycompany.projectdcoffee.model.Fee;
import java.util.List;

/**
 *
 * @author uSeR
 */
public class FeeService {
    public List<Fee> getFees(){
        FeeDao feeDao = new FeeDao();
        return feeDao.getAll(" fee_id asc");
    }

    public Fee addNew(Fee editedFee) {
        FeeDao feeDao = new FeeDao();
        return feeDao.save(editedFee);
    }

    public Fee update(Fee editedFee) {
        FeeDao feeDao = new FeeDao();
        return feeDao.update(editedFee);
    }

    public int delete(Fee editedFee) {
        FeeDao feeDao = new FeeDao();
        return feeDao.delete(editedFee);
    }

}
