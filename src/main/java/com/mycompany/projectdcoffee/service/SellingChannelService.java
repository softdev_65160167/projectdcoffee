/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.SellingChannelDao;
import com.mycompany.projectdcoffee.model.SellingChannel;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SellingChannelService {
    
    
    public List<SellingChannel> getSellingChannel(){
        SellingChannelDao scd = new SellingChannelDao();
        return scd.getAll(" sc_id asc");
    }

    public SellingChannel addNew(SellingChannel editedSellingChannel) {
        SellingChannelDao scd = new SellingChannelDao();
        return scd.save(editedSellingChannel);
    }

    public SellingChannel update(SellingChannel editedSellingChannel) {
        SellingChannelDao scd = new SellingChannelDao();
        return scd.update(editedSellingChannel);
    }

    public int delete(SellingChannel editedSellingChannel) {
        SellingChannelDao scd = new SellingChannelDao();
        return scd.delete(editedSellingChannel);
    }
}
