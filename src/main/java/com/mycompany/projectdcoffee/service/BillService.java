/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.projectdcoffee.service;

import com.mycompany.projectdcoffee.dao.BillDao;
import com.mycompany.projectdcoffee.dao.BillDetailDao;
import com.mycompany.projectdcoffee.dao.BillDao;
import com.mycompany.projectdcoffee.dao.BillDetailDao;
import com.mycompany.projectdcoffee.model.Bill;
import com.mycompany.projectdcoffee.model.BillDetail;
import com.mycompany.projectdcoffee.model.Bill;
import com.mycompany.projectdcoffee.model.BillReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADMIN
 */
public class BillService {
    public Bill getById(int id) {
        BillDao billDao = new BillDao();
        return billDao.get(id);
    }
    
    public ArrayList<Bill> getAll() {
        BillDao bill = new BillDao();
        return (ArrayList<Bill>) bill.getAll();
    }
    
    public List<Bill> getBills(){
        BillDao billDao = new BillDao();
        return billDao.getAll(" bill_id asc");
    }

    public Bill addNew(Bill editedBill) {
        BillDao billDao = new BillDao();
        BillDetailDao billDetailDao = new BillDetailDao();
        Bill bill = billDao.save(editedBill);
        for(BillDetail bd : editedBill.getBillDetails()){
            bd.setBillId(bill.getId());
            billDetailDao.save(bd);
        }
        return bill;
    }

    public Bill update(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        BillDao billDao = new BillDao();
        return billDao.delete(editedBill);
    }
    
    public List<BillReport> getTopTenArtistByTotalPrice() {
        BillDao artistDao = new BillDao();
        return artistDao.getArtistByTotalPrice(10);
    }
    
    public List<BillReport> getTopTenArtistByTotalPrice(String begin, String end) {
        BillDao artistDao = new BillDao();
        return artistDao.getArtistByTotalPrice(begin, end, 10);
    }

}


