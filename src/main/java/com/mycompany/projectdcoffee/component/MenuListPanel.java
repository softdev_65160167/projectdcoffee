/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.projectdcoffee.component;


import com.mycompany.projectdcoffee.model.Menu;
import com.mycompany.projectdcoffee.service.MenuService;
import java.awt.GridLayout;
import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class MenuListPanel extends javax.swing.JPanel implements BuyMenutable {

    private final MenuService menuService;
    private final ArrayList<Menu> menus;
    private ArrayList<BuyMenutable> subscribers = new ArrayList<>();

    /**
     * Creates new form MenuListPanel
     */
    public MenuListPanel() {
        initComponents();
        menuService = new MenuService();
        menus = menuService.getMenusCategoryDrink();
        int menuSize = menus.size();
        for (Menu p : menus) {
            MenuItemPanel pnlMenuItem = new MenuItemPanel(p);
            pnlMenuItem.addOnBuyMenu(this);
            pnlMenuList.add(pnlMenuItem);
        }
        pnlMenuList.setLayout(new GridLayout((menuSize / 3) + ((menuSize % 3 != 0) ? 1 : 0), 3, 0, 0));
    }

    public void addOnBuyMenu(BuyMenutable subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlMenuList = new javax.swing.JPanel();

        javax.swing.GroupLayout pnlMenuListLayout = new javax.swing.GroupLayout(pnlMenuList);
        pnlMenuList.setLayout(pnlMenuListLayout);
        pnlMenuListLayout.setHorizontalGroup(
            pnlMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 761, Short.MAX_VALUE)
        );
        pnlMenuListLayout.setVerticalGroup(
            pnlMenuListLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 529, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlMenuList, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel pnlMenuList;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Menu menu, int qty) {
        System.out.println("" + menu.getName() + " " + qty);
        for (BuyMenutable s : subscribers) {
            s.buy(menu, qty);
        }
    }
}
